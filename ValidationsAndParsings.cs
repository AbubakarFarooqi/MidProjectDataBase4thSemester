﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace MidProject
{
    public static class ValidationsAndParsings
    {
        // this fucntion check whther there is number in a given stirng
        public static bool isNumber(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (!Char.IsDigit(s[i]))
                {
                    return false;
                }
            }
            return true;
        }
        // this function will return number of month
        public static int parseMonthNumber(string month)
        {
            if (month == "January")
                return 1;
            if (month == "February")
                return 2;
            if (month == "March")
                return 3;
            if (month == "April")
                return 4;
            if (month == "May")
                return 5;
            if (month == "June")
                return 6;
            if (month == "July")
                return 7;
            if (month == "August")
                return 8;
            if (month == "September")
                return 9;
            if (month == "October")
                return 10;
            if (month == "November")
                return 11;
            if (month == "December")
                return 12;
            return -1;
        }

        public static bool isEveryFieldSet(Guna.UI2.WinForms.Guna2TextBox[] TXTs, ErrorProvider[] EPs, int size)
        {
            bool ret = true;
            for (int i = 0; i < size; i++)
            {
                if (TXTs[i].Text.Length == 0)
                {
                    ret = false;
                    EPs[i].SetError(TXTs[i], Messages.NOT_EMPTY);
                }
            }

            /* if (txtFirstName.Text.Length == 0)
             {
                 ret = false;
                 epFirstName.SetError(txtFirstName, "Field Cannot be Left Empty");
             }
             if (txtLastName.Text.Length == 0)
             {

                 ret = false;
                 epLastName.SetError(txtLastName, "Field Cannot be Left Empty");
             }
             if (txtContact.Text.Length == 0)
             {
                 ret = false;
                 epContact.SetError(txtContact, "Field Cannot be Left Empty");
             }
             if (txtEmail.Text.Length == 0)
             {
                 ret = false;
                 epEmail.SetError(txtEmail, "Field Cannot be Left Empty");
             }
             if (txtRegNo.Text.Length == 0)
             {
                 ret = false;
                 epRegNo.SetError(txtRegNo, "Field Cannot be Left Empty");
             }*/
            return ret;
        }
        //this funtion will parse date 
        public static string parseDate(string date)
        {
            string[] temp = date.Split(',');
            string year = temp[2];
            string[] monthAndDay = temp[1].Split(' ');
            string month = monthAndDay[1];
            string day = monthAndDay[2];
            return year + "-" + parseMonthNumber(month) + "-" + day;

        }
    }
}
