﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;

namespace MidProject
{
    public partial class formAssignProjectToGroup : Form
    {
        GroupDL groupDL;
        ProjectDL projectDL;
        AdvisorDL advisorDL;
        GroupProjectDL groupProjectDL;
        ProjectAdvisorDL projectAdvisorDL;
        string groupId;
        string projectId;
        string mainAdvisorId = null;
        string coAdvisorId = null;
        string industryAdvisorId = null;
        public formAssignProjectToGroup()
        {
            groupDL = new GroupDL();
            projectDL = new ProjectDL();
            advisorDL = new AdvisorDL();
            groupProjectDL = new GroupProjectDL();
            projectAdvisorDL = new ProjectAdvisorDL();
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formAssignProjectToGroup_Load(object sender, EventArgs e)
        {
            DGV_group.MultiSelect = false;
            DGV_project.MultiSelect = false;
            btnSelectProject.Visible = false;
            cbxCoAdvisor.Visible = false;
            cbxMainAdvisor.Visible = false;
            cbxIndustryAdvisor.Visible = false;
            btnAssign.Visible = false;
            btnSelectAdvisor.Visible = false;
            lblCoAdvisor.Visible = false;
            lblMainAdvisor.Visible = false;
            lblIndustryAdvisor.Visible = false;
            if (!groupDL.fetchNonGroupStudents())
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
            }
            DGV_group.DataSource = groupDL.getList();


        }
        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSelectGroup_Click(object sender, EventArgs e)
        {
            if (DGV_group.RowCount == 0)
                return;
            int rowIdx = DGV_group.SelectedRows[0].Index;
            if (!projectDL.fetchNonGroupProjects())
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
            }
            else
            {
                DGV_project.DataSource = projectDL.getList();
                btnSelectProject.Visible = true;
                DGV_group.Enabled = false;
                groupId = DGV_group.Rows[rowIdx].Cells["Id"].Value.ToString();
                btnSelectGroup.Enabled = false;
                btnSelectGroup.DisabledState.FillColor = Color.Firebrick;
                btnSelectGroup.DisabledState.ForeColor = Color.Black;
                btnSelectGroup.DisabledState.FillColor2 = Color.Black;
                btnSelectGroup.BackColor = Color.Black;
                btnSelectGroup.ForeColor = Color.Black;
            }
        }

        private void cbxDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnSelectProject_Click(object sender, EventArgs e)
        {

            if (DGV_project.RowCount == 0)
                return;
            int rowIdx = DGV_project.SelectedRows[0].Index;
            projectId = DGV_project.Rows[rowIdx].Cells["Id"].Value.ToString();
            DGV_project.Enabled = false;
            cbxCoAdvisor.Visible = true;
            cbxMainAdvisor.Visible = true;
            cbxIndustryAdvisor.Visible = true;
            cbxCoAdvisor.Enabled = false;
            cbxIndustryAdvisor.Enabled = false;
            lblCoAdvisor.Visible = true;
            lblMainAdvisor.Visible = true;
            lblIndustryAdvisor.Visible = true;
            if (advisorDL.fetchAdvisorsForProject())
            {
                cbxMainAdvisor.DataSource = advisorDL.getAdvisorListForProject();
                btnSelectAdvisor.Text = "Select Main Advisor";
                btnSelectAdvisor.Visible = true;
                btnSelectProject.Enabled = false;

                btnSelectProject.DisabledState.FillColor = Color.Firebrick;
                btnSelectProject.DisabledState.ForeColor = Color.Black;
                btnSelectProject.DisabledState.FillColor2 = Color.Black;

            }
            else
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
            }
        }

        private void btnSelectAdvisor_Click(object sender, EventArgs e)
        {
            if (mainAdvisorId == null)
            {
                string[] txt = cbxMainAdvisor.Text.Split('-');
                mainAdvisorId = txt[0];
                List<string> lst = new List<string>();
                //cbxMainAdvisor.Items.RemoveAt(cbxMainAdvisor.SelectedIndex);
                lst = (List<string>)cbxMainAdvisor.DataSource;
                lst.RemoveAt(cbxMainAdvisor.SelectedIndex);
                cbxCoAdvisor.DataSource = lst;
                cbxCoAdvisor.Enabled = true;
                cbxMainAdvisor.Enabled = false;
                btnSelectAdvisor.Text = "Select Co Advisor";
            }
            else if (coAdvisorId == null)
            {
                string[] txt = cbxCoAdvisor.Text.Split('-');
                coAdvisorId = txt[0];
                List<string> lst = new List<string>();
                lst = (List<string>)cbxCoAdvisor.DataSource;
                lst.RemoveAt(cbxCoAdvisor.SelectedIndex);
                cbxIndustryAdvisor.DataSource = lst;
                cbxIndustryAdvisor.Enabled = true;
                cbxCoAdvisor.Enabled = false;
                btnSelectAdvisor.Text = "Select Industry Advisor";
            }
            else if (industryAdvisorId == null)
            {
                string[] txt = cbxIndustryAdvisor.Text.Split('-');
                industryAdvisorId = txt[0];
                cbxIndustryAdvisor.Enabled = false;
                btnSelectAdvisor.Enabled = false;

                btnSelectAdvisor.DisabledState.FillColor = Color.Firebrick;
                btnSelectAdvisor.DisabledState.ForeColor = Color.Black;
                btnSelectAdvisor.DisabledState.FillColor2 = Color.Black;
                btnAssign.Visible = true;
            }
        }

        private void btnAssign_Click(object sender, EventArgs e)
        {
            if (mainAdvisorId.Length == 0 || coAdvisorId.Length == 0 || industryAdvisorId.Length == 0)
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.NO_ADVISOR_SELECT;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
            }
            else
            {
                DateTime createdOn = new DateTime();
                createdOn = DateTime.Now;
                GroupProject gp = new GroupProject(projectId, groupId, createdOn.ToString());
                if (!groupProjectDL.insert(gp))
                {

                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.FAIL_INSERT;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                }
                else
                {
                    bool success = false;
                    ProjectAdvisor pa = new ProjectAdvisor(mainAdvisorId, projectId, "Main Advisor", createdOn.ToString());
                    if (projectAdvisorDL.insert(pa))
                    {
                        pa = new ProjectAdvisor(coAdvisorId, projectId, "Co-Advisor", createdOn.ToString());

                        if (projectAdvisorDL.insert(pa))
                        {
                            pa = new ProjectAdvisor(industryAdvisorId, projectId, "Industry Advisor", createdOn.ToString());
                            if (projectAdvisorDL.insert(pa))
                            {
                                success = true;
                            }
                        }
                    }
                    if (success)
                    {
                        msgBox.Caption = Messages.CAPTION_SUCCESS;
                        msgBox.Text = Messages.SUCCESS_INSERT;
                        msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                        msgBox.Show();
                        this.Close();
                    }
                    else
                    {
                        msgBox.Caption = Messages.CAPTION_FAIL;
                        msgBox.Text = Messages.FAIL_INSERT;
                        msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                        msgBox.Show();
                    }
                }

            }
        }
    }
}
