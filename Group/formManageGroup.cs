﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formManageGroup : Form
    {
        formSearchDialog formSearch;
        Panel pnlChildForm;
        GroupDL groupDL;
        public formManageGroup(Panel PnlchildForm)
        {
            this.pnlChildForm = PnlchildForm;
            groupDL = new GroupDL();
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            formSearch = new formSearchDialog("Enter Group Id");
            formSearch.searchClick += new EventHandler(onSearch);
            formSearch.ShowDialog();
        }

        private void onSearch(object sender, EventArgs e)
        {
            bool isFound = false;
            string ID = formSearch.RegNo; //  form return ID
            for (int i = 0; i < DGV.RowCount; i++)
            {
                // finding row that contain reg no
                if (DGV.Rows[i].Cells["Id"].Value.ToString() == ID)
                {
                    DGV.ClearSelection();
                    DGV.Rows[i].Selected = true;
                    sbVertical.Value = i + 1;
                    isFound = true;
                    break;
                }
            }
            formSearch.isFound = isFound;
        }

        private void formManageGroup_Load(object sender, EventArgs e)
        {
            DGV.MultiSelect = false;
            DGV.ScrollBars = ScrollBars.None;
            if (!groupDL.fetchRecords())
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
                return;
            }
            else
            {

            }
            DGV.DataSource = groupDL.getList();
            changeDGVorder();

        }
        private void changeDGVorder()
        {
            DGV.AutoGenerateColumns = false;
            DGV.Columns["Id"].DisplayIndex = 0;
            DGV.Columns["Created_On"].DisplayIndex = 1;
        }
        private void openChildForm(Form childForm)
        {
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            pnlChildForm.Controls.Add(childForm);
            pnlChildForm.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
        private void btnManageStudents_Click(object sender, EventArgs e)
        {
            int rowIdx = DGV.SelectedRows[0].Index;
            string groupId = DGV.Rows[rowIdx].Cells["Id"].Value.ToString();
            openChildForm(new formManageGroupStudents(groupId));
        }

        /*  private void btnDelete_Click(object sender, EventArgs e)
          {
              int rowIdx = DGV.SelectedRows[0].Index;
              if (DGV.RowCount == 0)
                  return;
              DialogResult result = MessageBox.Show("Are You Sure to Delete " + DGV.SelectedRows[0].Cells[0].Value.ToString(), "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
              if (result == DialogResult.Yes)
              {
                  if (DataBase.deleteAllGroupStudent(DGV.Rows[rowIdx].Cells[0].Value.ToString()))
                  {
                      if (!DataBase.deleteGroup(DGV.Rows[rowIdx].Cells[0].Value.ToString()))
                      {
                          MessageBox.Show("Something Went Wrong while Fetching Data Form DataBase" +
                                      "Kindly check your Connect", "Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                      }
                  }
                  else
                  {
                      MessageBox.Show("Something Went Wrong while Fetching Data Form DataBase" +
                                      "Kindly check your Connect", "Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                  }
                  DataBase.fillManageGroupDGV(DGV);
              }
          }*/
    }
}
