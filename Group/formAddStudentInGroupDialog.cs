﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{

    public partial class formAddStudentInGroupDialog : Form
    {
        StudentDL studentDL;
        GroupStudentDL groupStudentDl;
        List<int> selectedRows;
        string groupId;
        public formAddStudentInGroupDialog(string groupId)
        {
            InitializeComponent();
            selectedRows = new List<int>();
            studentDL = new StudentDL();
            groupStudentDl = new GroupStudentDL();
            this.groupId = groupId;
        }
        private void bindData()
        {

            if (!studentDL.fetchNonGroupStudents())
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();

            }
            List<Student> lstStd = studentDL.getList();
            /* List<Student> idx = new List<Student>();
             for (int i = 0; i < lstStd.Count; i++)
             {
                 if (groupStudentDl.isGroupStudent(lstStd[i].Id))
                     idx.Add(lstStd[i]);
             }
             for (int i = 0; i < idx.Count; i++)
             {
                 lstStd.Remove(idx[i]);
             }
             if (DGV.DataSource != null)
                 DGV.Columns.Remove("chkBoxSelected");*/
            DGV.DataSource = lstStd;
            changeDGVorder();
        }
        private void changeDGVorder()
        {
            DGV.AutoGenerateColumns = false;
            DGV.Columns["Id"].DisplayIndex = 0;
            DGV.Columns["RegistrationNumber"].DisplayIndex = 1;
            DGV.Columns["FirstName"].DisplayIndex = 2;
            DGV.Columns["LastName"].DisplayIndex = 3;
            DGV.Columns["Contact"].DisplayIndex = 4;
            DGV.Columns["Email"].DisplayIndex = 5;


            DGV.Columns["DateOfBirth"].DisplayIndex = 6;
            DGV.Columns["Gender"].DisplayIndex = 7;
            DGV.Columns["RegistrationNumber"].HeaderText = "Registration Number";
            DGV.Columns["FirstName"].HeaderText = "First Name";
            DGV.Columns["LastName"].HeaderText = "Last Name";

            DGV.Columns["DateOfBirth"].Visible = false;
            DGV.Columns["Gender"].Visible = false;
            DataGridViewCheckBoxColumn clm = new DataGridViewCheckBoxColumn();
            clm.ValueType = typeof(bool);
            clm.Name = "chkBoxSelected";
            clm.HeaderText = "Selected";
            DGV.Columns.Add(clm);
            DGV.ReadOnly = false;
            foreach (DataGridViewColumn column in DGV.Columns)
            {
                column.ReadOnly = true;
            }
            DGV.Columns["chkBoxSelected"].ReadOnly = false;
        }
        private void DataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            //Check to ensure that the row CheckBox is clicked.
            if (e.RowIndex >= 0/* && e.ColumnIndex == 0*/)
            {
                //Reference the GridView Row.
                DataGridViewRow row = DGV.Rows[e.RowIndex];

                //Set the CheckBox selection.
                row.Cells["chkBoxSelected"].Value = Convert.ToBoolean(row.Cells["chkBoxSelected"].EditedFormattedValue);

                //If CheckBox is checked, display Message Box.
                if (Convert.ToBoolean(row.Cells["chkBoxSelected"].Value))
                {
                    selectedRows.Add(e.RowIndex);
                }
                if (!Convert.ToBoolean(row.Cells["chkBoxSelected"].Value))
                {
                    selectedRows.Remove(e.RowIndex);
                }
            }
        }
        private void formAddStudentInGroupDialog_Load(object sender, EventArgs e)
        {
            DGV.MultiSelect = false;
            DGV.ScrollBars = ScrollBars.None;
            DGV.CellContentClick += new DataGridViewCellEventHandler(DataGridView_CellClick);
            bindData();
        }
        private void guna2GradientButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {


            if (selectedRows.Count == 0)
            {

                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.NO_STUDENT_SELECT;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                return;
            }

            DateTime createdOn = new DateTime();
            createdOn = DateTime.Now;


            bool success = true;
            foreach (var i in selectedRows)
            {
                string studentId = DGV.Rows[i].Cells["Id"].Value.ToString();
                GroupStudent groupStudent = new GroupStudent(studentId, "Active", createdOn.ToString(), groupId);
                if (!groupStudentDl.insert(groupStudent))
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.FAIL_INSERT;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    success = false;
                    break;
                }
            }
            if (success)
            {
                msgBox.Caption = Messages.CAPTION_SUCCESS;
                msgBox.Text = Messages.SUCCESS_INSERT;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                msgBox.Show();
                bindData();
                selectedRows.Clear();
            }
            this.Close();

            /*
            if (selectedRows.Count == 0)
            {
                MessageBox.Show("Please Select Students", "Caution", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            DateTime createdOn = new DateTime();
            createdOn = DateTime.Now;
            bool success = true;
            foreach (var i in selectedRows)
            {
                string studentId = DGV.Rows[i].Cells[0].Value.ToString();
                if (!DataBase.insertGroupStudent(groupId, studentId, "Active", createdOn.ToString()))
                {
                    MessageBox.Show("SomeThing Went Wrong. Kindly check your connection credentials", "Failure",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    success = false;
                    break;
                }
            }
            if (success)
            {
                MessageBox.Show("Student Added Successfully", "Success",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                selectedRows.Clear();
                this.Close();
            }*/
        }
    }
}
