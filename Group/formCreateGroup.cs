﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formCreateGroup : Form
    {
        formSearchDialog formSearch;
        List<int> selectedRows;
        GroupDL groupDL;
        GroupStudentDL groupStudentDl;
        StudentDL studentDL;
        public formCreateGroup()
        {
            InitializeComponent();
            selectedRows = new List<int>();
            groupDL = new GroupDL();
            groupStudentDl = new GroupStudentDL();
            studentDL = new StudentDL();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tblView_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

        }

        private void formCreateGroup_Load(object sender, EventArgs e)
        {
            DGV.MultiSelect = false;
            DGV.ScrollBars = ScrollBars.None;
            DGV.CellContentClick += new DataGridViewCellEventHandler(DataGridView_CellClick);
            bindData();
        }

        private void bindData()
        {
            DGV.DataSource = null;
            if (!studentDL.fetchNonGroupStudents())
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();

            }
            List<Student> lstStd = studentDL.getList();
            /* List<Student> idx = new List<Student>();
             for (int i = 0; i < lstStd.Count; i++)
             {
                 if (groupStudentDl.isGroupStudent(lstStd[i].Id))
                     idx.Add(lstStd[i]);
             }
             for (int i = 0; i < idx.Count; i++)
             {
                 lstStd.Remove(idx[i]);
             }
             if (DGV.DataSource != null)
                 DGV.Columns.Remove("chkBoxSelected");*/
            DGV.DataSource = lstStd;
            changeDGVorder();
        }
        private void changeDGVorder()
        {
            DGV.AutoGenerateColumns = false;
            DGV.Columns["Id"].DisplayIndex = 0;
            DGV.Columns["RegistrationNumber"].DisplayIndex = 1;
            DGV.Columns["FirstName"].DisplayIndex = 2;
            DGV.Columns["LastName"].DisplayIndex = 3;
            DGV.Columns["Contact"].DisplayIndex = 4;
            DGV.Columns["Email"].DisplayIndex = 5;


            DGV.Columns["DateOfBirth"].DisplayIndex = 6;
            DGV.Columns["Gender"].DisplayIndex = 7;
            DGV.Columns["RegistrationNumber"].HeaderText = "Registration Number";
            DGV.Columns["FirstName"].HeaderText = "First Name";
            DGV.Columns["LastName"].HeaderText = "Last Name";

            DGV.Columns["DateOfBirth"].Visible = false;
            DGV.Columns["Gender"].Visible = false;
            DataGridViewCheckBoxColumn clm = new DataGridViewCheckBoxColumn();
            clm.ValueType = typeof(bool);
            clm.Name = "chkBoxSelected";
            clm.HeaderText = "Selected";
            DGV.Columns.Add(clm);
            DGV.ReadOnly = false;
            foreach (DataGridViewColumn column in DGV.Columns)
            {
                column.ReadOnly = true;
            }
            DGV.Columns["chkBoxSelected"].ReadOnly = false;
        }

        private void DataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            //Check to ensure that the row CheckBox is clicked.
            if (e.RowIndex >= 0/* && e.ColumnIndex == 0*/)
            {
                //Reference the GridView Row.
                DataGridViewRow row = DGV.Rows[e.RowIndex];

                //Set the CheckBox selection.
                row.Cells["chkBoxSelected"].Value = Convert.ToBoolean(row.Cells["chkBoxSelected"].EditedFormattedValue);

                //If CheckBox is checked, display Message Box.
                if (Convert.ToBoolean(row.Cells["chkBoxSelected"].Value))
                {
                    selectedRows.Add(e.RowIndex);
                }
                if (!Convert.ToBoolean(row.Cells["chkBoxSelected"].Value))
                {
                    selectedRows.Remove(e.RowIndex);
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            formSearch = new formSearchDialog("Enter Group Id");
            formSearch.searchClick += new EventHandler(onSearch);
            formSearch.ShowDialog();
        }

        private void onSearch(object sender, EventArgs e)
        {
            bool isFound = false;
            string RegNo = formSearch.RegNo; //  form return ID
            for (int i = 0; i < DGV.RowCount; i++)
            {
                // finding row that contain reg no
                if (DGV.Rows[i].Cells["Id"].Value.ToString() == RegNo)
                {
                    DGV.ClearSelection();
                    DGV.Rows[i].Selected = true;
                    sbVertical.Value = i + 1;
                    isFound = true;
                    break;
                }
            }
            formSearch.isFound = isFound;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (selectedRows.Count == 0)
            {

                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.NO_STUDENT_SELECT;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                return;
            }
            DateTime createdOn = new DateTime();
            createdOn = DateTime.Now;
            Group group = new Group(createdOn.ToString());
            string groupId = groupDL.insert(group);
            //groupId = DataBase.insertGroup(createdOn.ToString());
            if (groupId == null)
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_INSERT;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                return;
            }
            bool success = true;
            foreach (var i in selectedRows)
            {
                string studentId = DGV.Rows[i].Cells["Id"].Value.ToString();
                GroupStudent groupStudent = new GroupStudent(studentId, "Active", createdOn.ToString(), groupId);
                if (!groupStudentDl.insert(groupStudent))
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.FAIL_INSERT;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    success = false;
                    break;
                }
            }
            if (success)
            {
                msgBox.Caption = Messages.CAPTION_SUCCESS;
                msgBox.Text = Messages.SUCCESS_INSERT;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                msgBox.Show();
                DGV.Columns.Remove("chkBoxSelected");
                bindData();
                selectedRows.Clear();
            }

        }
    }
}
