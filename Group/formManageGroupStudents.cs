﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formManageGroupStudents : Form
    {
        string groupId;
        GroupStudentDL groupStudentDL;
        List<int> inActiveRows;
        public formManageGroupStudents(string groupId)
        {
            this.groupId = groupId;
            groupStudentDL = new GroupStudentDL();
            inActiveRows = new List<int>();
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void DataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            //Check to ensure that the row CheckBox is clicked.
            if (e.RowIndex >= 0/* && e.ColumnIndex == 0*/)
            {
                //Reference the GridView Row.
                DataGridViewRow row = DGV.Rows[e.RowIndex];

                //Set the CheckBox selection.
                row.Cells["chkBoxActive"].Value = !Convert.ToBoolean(row.Cells["chkBoxActive"].EditedFormattedValue);

                //If CheckBox is checked, display Message Box.
                if (Convert.ToBoolean(row.Cells["chkBoxActive"].Value))
                {
                    inActiveRows.Remove(e.RowIndex);
                }
                if (!Convert.ToBoolean(row.Cells["chkBoxActive"].Value))
                {
                    inActiveRows.Add(e.RowIndex);
                }
            }
        }
        private void formManageGroupStudents_Load(object sender, EventArgs e)
        {
            DGV.MultiSelect = false;
            DGV.ScrollBars = ScrollBars.None;
            DGV.CellContentClick += new DataGridViewCellEventHandler(DataGridView_CellClick);
            DataTable dt = groupStudentDL.fetchRecords(groupId);
            if (dt == null)
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
            }
            groupStudentDL.fetchRecords(groupId);
            //  DGV.DataSource = groupStudentDL.getList();
            //DGV.DataSource = groupStudentDL.getList();
            bindData(dt);

            //  if (!DataBase.fillManageGroupStudentDGV(DGV, groupId))
            /* {
                MessageBox.Show("Something Went wrong while " +
                                 " information from DataBase. Kindly check your connection",
                                    "Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnUpdate.Enabled = false;
            }*/
        }
        private void bindData(DataTable dt)
        {
            dt.Columns.Add("chkBoxActive", typeof(bool));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["chkBoxActive"] = true;

            }
            DGV.DataSource = dt;
            foreach (DataGridViewColumn column in DGV.Columns)
            {
                column.ReadOnly = true;
            }
            DGV.Columns["chkBoxActive"].ReadOnly = false;
            /*//DGV.AutoGenerateColumns = false;
            DGV.Columns["Id"].DisplayIndex = 0;
            DGV.Columns["FirstName"].DisplayIndex = 1;
            DGV.Columns["LastName"].DisplayIndex = 2;
            DGV.Columns.Remove(DGV.Columns["Contact"]);
            DGV.Columns.Remove(DGV.Columns["Email"]);
            DGV.Columns.Remove(DGV.Columns["DateOfBirth"]);
            DGV.Columns.Remove(DGV.Columns["Gender"]);
            DGV.Columns.Remove(DGV.Columns["RegistrationNumber"]);
            DGV.Columns["FirstName"].HeaderText = "First Name";
            DGV.Columns["LastName"].HeaderText = "Last Name";
            MessageBox.Show("a");
            DataGridViewCheckBoxColumn clm = new DataGridViewCheckBoxColumn();
            clm.ValueType = typeof(bool);
            clm.Name = "chkBoxActive";
            clm.HeaderText = "Active";
            DGV.Columns.Add(clm);
            DGV.ReadOnly = false;
            foreach (DataGridViewColumn column in DGV.Columns)
            {
                column.ReadOnly = true;
            }
            DGV.Columns["chkBoxActive"].ReadOnly = false;
            DGV.ClearSelection();
            for (int i = 0; i < DGV.Rows.Count; i++)
            {
                DGV.Rows[i].Cells["chkBoxActive"].Selected = true;
                ((DataGridViewCheckBoxCell)DGV.Rows[i].Cells["chkBoxActive"]).Value = true;
                MessageBox.Show(DGV.Rows[i].Cells["chkBoxActive"].Value.ToString());
                ((DataGridViewCheckBoxCell)DGV.Rows[i].Cells["chkBoxActive"]).Value = true;
                MessageBox.Show(DGV.Rows[i].Cells["chkBoxActive"].Value.ToString());
                DGV.CommitEdit(DataGridViewDataErrorContexts.Commit);
                DGV.Update();

            }*/


        }
        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            formAddStudentInGroupDialog form = new formAddStudentInGroupDialog(groupId);
            form.ShowDialog();
            this.Close();
            DataBase.fillManageGroupStudentDGV(DGV, groupId);
        }

        private void btnUpdate_Click_1(object sender, EventArgs e)
        {
            if (inActiveRows.Count == 0)
                return;
            for (int i = 0; i < DGV.RowCount; i++)
            {
                if (DGV.Rows[i].Cells["chkBoxActive"].Value.ToString() == "False")
                {
                    if (!DataBase.updateGroupStudentStatus(false, DGV.Rows[i].Cells[0].Value.ToString()))
                        goto Error;
                }
            }
            msgBox.Caption = Messages.CAPTION_SUCCESS;
            msgBox.Text = Messages.SUCCESS_UPDATE;
            msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
            msgBox.Show();
            this.Close();
            goto End;
        // if (DataBase.updateGroupStudent(groupId , studentId , status))
        Error:;
            msgBox.Caption = Messages.CAPTION_FAIL;
            msgBox.Text = Messages.FAIL_UPDATE;
            msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
            msgBox.Show();
        End:;
        }
    }
}
