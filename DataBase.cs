﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Data;
using Guna.UI2.WinForms;

namespace MidProject
{
    public static class DataBase
    {
        public static SqlConnection con = Configuration.getInstance().getConnection();


        // INSERT QUERIES..............................................

        public static bool insertProjectAdvisor(string advisorId, string projectId, string role, string date)
        {
            int roleId = getAdvisorRole(role);
            if (roleId == -1)
                return false;
            try
            {

                SqlCommand cmd = new SqlCommand("insert into ProjectAdvisor values(" +
                                            "@advisorId ,@projectId ,@role , @date )", con);
                cmd.Parameters.AddWithValue("@advisorId", advisorId);
                cmd.Parameters.AddWithValue("@projectId", projectId);
                cmd.Parameters.AddWithValue("@role", roleId);
                cmd.Parameters.AddWithValue("@date", date);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool insertGroupProject(string groupId, string projectId, string date)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("insert into GroupProject values(@projectId , @groupId , @date)", con);
                cmd.Parameters.AddWithValue("@groupId", groupId);
                cmd.Parameters.AddWithValue("@projectId", projectId);
                cmd.Parameters.AddWithValue("@date", date);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ee)
            {
                MessageBox.Show(ee.Number.ToString());
                return false;
            }
        }
        public static bool insertProject(string description, string title)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("insert into Project values(@description,@title)", con);
                cmd.Parameters.AddWithValue("@description", description);
                cmd.Parameters.AddWithValue("@title", title);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool insertStudent(string firstName, string lastName, string contact, string email,
                                            string DOB, string gender, string regNo)
        {
            int genderId = getGender(gender);
            if (genderId != -1)
            {
                string id = insertPerson(firstName, lastName, contact, email, DOB, genderId);
                if (id != "-1")
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand("insert into Student values (@id ,@RegNo)", con);
                        cmd.Parameters.AddWithValue("@RegNo", regNo);
                        cmd.Parameters.AddWithValue("@id", id);
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                    catch (SqlException ex)
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }


        }


        private static string insertPerson(string FirstName, string LastName, string Contact, string Email,
                                            string DOB, int gender)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("insert person OUTPUT inserted.id values(@FirstName,@LastName,@Contact,@Email,@DOB,@Gender) ", con);
                cmd.Parameters.AddWithValue("@FirstName", FirstName);
                cmd.Parameters.AddWithValue("@LastName", LastName);
                cmd.Parameters.AddWithValue("@Contact", Contact);
                cmd.Parameters.AddWithValue("@Email", Email);
                cmd.Parameters.AddWithValue("@DOB", ValidationsAndParsings.parseDate(DOB));
                cmd.Parameters.AddWithValue("@Gender", gender);
                string id = cmd.ExecuteScalar().ToString();
                return id;
            }
            catch (SqlException ex)
            {
                return "-1";
            }

        }
        public static bool insertAdvisor(string firstName, string lastName, string contact, string email,
                                          string DOB, string gender, string designation, string salary)
        {
            int genderId = getGender(gender);
            int designationId = getDesignation(designation);

            if (genderId == -1 || designationId == -1)
                return false;

            string id = insertPerson(firstName, lastName, contact, email,
                                             DOB, genderId);

            if (id == "-1")
                return false;
            try
            {
                SqlCommand cmd = new SqlCommand("insert into Advisor values (@id,@designation , @salary)", con);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@designation", designationId);
                cmd.Parameters.AddWithValue("@salary", salary);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }

        }
        public static string insertGroup(string createdOn)
        {
            SqlCommand cmd;

            string id;
            try
            {
                cmd = new SqlCommand("insert [Group] OUTPUT inserted.id  values (@date)", con);
                cmd.Parameters.AddWithValue("@date", createdOn);
                id = cmd.ExecuteScalar().ToString();
                return id;
            }
            catch (SqlException ex)
            {
                return null;
            }

        }
        public static bool insertGroupStudent(string groupId, string studentId, string status, string date)
        {
            int statusId = getStatus(status);
            if (statusId == -1)
                return false;
            try
            {
                SqlCommand cmd = new SqlCommand("insert into GroupStudent values(@gid ,@sid , @status , @date )", con);
                cmd.Parameters.AddWithValue("@gid", groupId);
                cmd.Parameters.AddWithValue("@sid", studentId);
                cmd.Parameters.AddWithValue("@status", statusId);
                cmd.Parameters.AddWithValue("@date", date);
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        //.....................................

        //DELETE Queries.....................

        public static bool deleteGroup(string groupId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("delete[Group] where Id = @groupId", con);
                cmd.Parameters.AddWithValue("@groupId", groupId);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool deleteGroupStudent(string groupId, string studentId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("delete GroupStudent where GroupId = @groupId AND StudentId = @sid", con);
                cmd.Parameters.AddWithValue("@groupId", groupId);
                cmd.Parameters.AddWithValue("@sid", studentId);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool deleteAllGroupStudent(string groupId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("delete GroupStudent where GroupId = @groupId", con);
                cmd.Parameters.AddWithValue("@groupId", groupId);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool deleteAdvisor(string id)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("delete Advisor where Id = @id", con);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            if (deletePerson(id))
                return true;
            return false;

        }
        public static bool deleteStudent(string regNo)
        {
            SqlCommand cmd;
            string id = "";
            try
            {
                cmd = new SqlCommand("select id from student where RegistrationNo = @regNo", con);
                cmd.Parameters.AddWithValue("@regNo", regNo);
                id = cmd.ExecuteScalar().ToString();
            }
            catch (SqlException ex)
            {
                return false;
            }

            try
            {
                cmd = new SqlCommand("delete student where RegistrationNo = @regNo", con);
                cmd.Parameters.AddWithValue("@regNo", regNo);
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                return false;
            }

            if (deletePerson(id))
                return true;
            return false;

        }
        private static bool deletePerson(string id)
        {

            try
            {
                SqlCommand cmd = new SqlCommand("delete  person where id = @id", con);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }

        }

        //.................................................

        //Update QUERIES.................................
        public static bool updateProject(string projectId, string title, string description)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Update Project set Title = @title ,Description = @desc where id = @id", con);
                cmd.Parameters.AddWithValue("@title", title);
                cmd.Parameters.AddWithValue("@desc", description);
                cmd.Parameters.AddWithValue("@id", projectId);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private static bool updatePerson(string ID, string FirstName, string LastName, string Contact, string Email,
                                            string DOB, int gender)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("update person set FirstName = @firstName, LastName = @lastName, Contact = @contact, Email = @email, DateOfBirth = @dob ,Gender =  @gender where id  = \'" + ID + "\'", con);
                cmd.Parameters.AddWithValue("@firstName", FirstName);
                cmd.Parameters.AddWithValue("@lastName", LastName);
                cmd.Parameters.AddWithValue("@contact", Contact);
                cmd.Parameters.AddWithValue("@email", Email);
                cmd.Parameters.AddWithValue("@dob", ValidationsAndParsings.parseDate(DOB));
                cmd.Parameters.AddWithValue("@gender", gender);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }

        }
        public static bool updateStudent(string newRegNo, string OldregNo, string FirstName, string LastName,
                                            string Contact, string Email, string DOB, string gender)
        {
            string id = "";
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("select id  from student where RegistrationNo =" + "\'" + OldregNo + "\'", con);
                id = cmd.ExecuteScalar().ToString();
            }
            catch (SqlException ex)
            {
                return false;
            }

            int genderId = getGender(gender);
            if (genderId == -1)
                return false;

            if (!updatePerson(id, FirstName, LastName, Contact, Email, DOB, genderId))
                return false;
            try
            {
                cmd = new SqlCommand("update student set RegistrationNo = @regNo where RegistrationNo = " + "\'" + OldregNo + "\'", con);
                cmd.Parameters.AddWithValue("@regNo", newRegNo);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }


        }
        public static bool updateAdvisor(string id, string FirstName, string LastName,
                                            string Contact, string Email, string DOB, string gender
                                                , string designation, string salary)
        {
            int genderId = getGender(gender);
            int designationId = getDesignation(designation);
            if (genderId == -1 || designationId == -1)
                return false;

            if (!updatePerson(id, FirstName, LastName, Contact, Email, DOB, genderId))
                return false;

            try
            {
                SqlCommand cmd = new SqlCommand("update advisor set Designation = @designation , " +
                     "Salary = @salary where  id = " + "\'" + id + "\'", con);
                cmd.Parameters.AddWithValue("@designation", designationId);
                cmd.Parameters.AddWithValue("@salary", salary);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool updateGroupStudentStatus(bool statusBool, string sid)
        {
            string statusStr = "";
            if (statusBool)
                statusStr = "Active";
            else
                statusStr = "InActive";
            try
            {
                int status = getStatus(statusStr);
                SqlCommand cmd = new SqlCommand("update GroupStudent set Status = @status where StudentId = @sid", con);
                cmd.Parameters.AddWithValue("@status", status);
                cmd.Parameters.AddWithValue("@sid", sid);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //Fill Queries ...............................
        public static bool fillStudentDGV(DataGridView DGV)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select s.RegistrationNo as [Registration Number] , FirstName " +
                    ", LastName , Contact , Email , DateOfBirth as [Date of Birth] " +
                    ", (select value from Lookup where id = p.Gender) as Gender from student s join person p  on s.id" +
                    " = p.id ", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DGV.DataSource = dt;
                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }
        public static bool fillAdvisorCBX(ComboBox cbx)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select CONCAT(a.Id, '-' ,FirstName , ' ' , LastName) as name from advisor a " +
                    "join person p on p.id = a.id", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                List<string> names = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    names.Add(dt.Rows[i][0].ToString());
                }
                cbx.DataSource = names;
                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }
        public static bool fillDesignationsCBX(ComboBox cbx)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select value from lookup where Category = 'DESIGNATION'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                List<string> designations = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    designations.Add(dt.Rows[i][0].ToString());
                }
                cbx.DataSource = designations;
                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }
        public static bool fillAdvisorDGV(DataGridView DGV)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select A.Id as [Advisor ID] , FirstName , LastName , Contact ," +
                    " Email , DateOfBirth as [Date of Birth] " +
                    ", (select value from Lookup where id = A.Designation) AS Designation ,A.salary, " +
                    "(select value from Lookup where id = p.Gender) as Gender from advisor A join person p  " +
                    "on A.id = p.id ", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DGV.DataSource = dt;
                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }

        }
        public static bool fillManageGroupStudentDGV(DataGridView DGV, string groupId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select StudentId , FirstName , LastName , [Status] from GroupStudent g join Person p on p.Id = g.StudentId where GroupId = @groupId", con);
                cmd.Parameters.AddWithValue("@groupId", groupId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dt.Columns.Add("isActive", typeof(bool));
                dt.Columns.Add("isSelected", typeof(bool));
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["isSelected"] = true;
                    if (int.Parse(dt.Rows[i].ItemArray[3].ToString()) == 3)
                        dt.Rows[i]["isActive"] = true;

                }
                dt.Columns.RemoveAt(3);
                DGV.DataSource = dt;
                DGV.ReadOnly = false;
                DGV.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopLeft;
                DGV.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopLeft;
                DGV.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopLeft;
                DGV.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                DGV.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                foreach (DataGridViewColumn column in DGV.Columns)
                {
                    column.ReadOnly = true;
                }
                DGV.Columns[3].ReadOnly = false;
                DGV.Columns[4].ReadOnly = false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool fillCreateGroupDGV(DataGridView DGV)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select s.Id ,RegistrationNo , R.FirstName , R.LastName " +
                    "from Student s " +
                    "join (select  id,firstName , LastName from person  where id in (select id from student s EXCEPT select StudentId from GroupStudent where Status = 3))R" +
                    " on R.Id = s.Id", con);


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DGV.DataSource = dt;
                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }
        public static bool fillProjectDGV(DataGridView DGV)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Select Id , Title , Description From Project", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DGV.DataSource = dt;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool fillManageGroupDGV(DataGridView DGV)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select Id as [Group Id] from[group]", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DGV.DataSource = dt;
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        //............................

        // PRIVATE METHODS.............................
        private static int getDesignation(string desination)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select id  from Lookup where Value = @designation", con);
                cmd.Parameters.AddWithValue("@designation", desination);
                int designationId = int.Parse(cmd.ExecuteScalar().ToString());
                return designationId;
            }
            catch (SqlException ex)
            {
                return -1;
            }

        }
        // private static ()
        private static int getStatus(string status)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select id  from Lookup where Value = @status", con);
                cmd.Parameters.AddWithValue("@status", status);
                int designationId = int.Parse(cmd.ExecuteScalar().ToString());
                return designationId;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }
        private static int getGender(string gender)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select id  from Lookup where Value = @Gender", con);
                cmd.Parameters.AddWithValue("@Gender", gender);
                int id = int.Parse(cmd.ExecuteScalar().ToString());
                return id;
            }
            catch (SqlException ex)
            {
                return -1;
            }


        }
        private static int getAdvisorRole(string role)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select id from Lookup where  value = @role", con);
                cmd.Parameters.AddWithValue("@role", role);
                int id = int.Parse(cmd.ExecuteScalar().ToString());
                return id;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        //................................................
    }
}
