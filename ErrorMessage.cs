﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidProject
{
    public static class Messages
    {

        public static string NOT_EMPTY = "Field Cannot Be Empty";
        public static string PROMPT_DELETE = "Are You Sure to Delete";
        public static string FAIL_INSERT = "Cannot Insert Data" +
                                            " Please Check Your " +
                                            "Connection";
        public static string FAIL_UPDATE = "Cannot Update Data" +
                                            " Please Check Your " +
                                            "Connection";
        public static string FAIL_DELETE = "Cannot Delete Data" +
                                            " Please Check Your " +
                                            "Connection";
        public static string SUCCESS_INSERT = "Data Has Been Successfully Inserted";
        public static string SUCCESS_DELETE = "Record Has Been successfully Deleted";
        public static string SUCCESS_UPDATE = "Data Has Been Successfully Updated";
        public static string CAPTION_SUCCESS = "Success";
        public static string CAPTION_FAIL = "Failure";
        public static string FAIL_RETRIEVE = "Cannot Fetch Records Kindly " +
                                                "Check Your Connection";
        public static string NO_STUDENT_SELECT = "Please Select Students";
        public static string NO_ADVISOR_SELECT = "Please Select All Advisors";
        public static string NOT_NUMBER = "Please Enter a valid Number";
        public static string NOT_ACCEPTABLE_WEIGHTAGE = "Total Weightage of All Evluations cannot be Greater than 100";
        public static string NOT_ACCEPTABLE_OBTAINED_MARKS = "Obtained Marks Must Be Smaller Than Total Marks";
        public static string REPORT_NOT_GENERATED = "Report Cannot Be Generated";
        public static string REPORT_GENERATED = "File Has Been Saved";

    }
}
