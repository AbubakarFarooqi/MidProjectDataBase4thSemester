﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;

namespace MidProject
{
    public partial class formUpdateAdvisorDialog : Form
    {
        string Id, designation, gender;
        Advisor adv;
        AdvisorDL advisorDL;

        public event EventHandler onSuccessUpdate;
        public formUpdateAdvisorDialog(Advisor adv)
        {
            InitializeComponent();
            this.adv = adv;
            advisorDL = new AdvisorDL();
            this.txtFirstName.Text = adv.FirstName;
            txtLastName.Text = adv.LastName;
            txtContact.Text = adv.Contact;
            txtEmail.Text = adv.Email;
            this.Id = adv.Id;
            txtSalary.Text = adv.Salary;
            this.designation = adv.Designation;
            cbxGender.Text = adv.Gender;
            tpDOB.Text = adv.DateOfBirth;


        }

        private void formUpdateAdvisorDialog_Load(object sender, EventArgs e)
        {
            DataTable dt = advisorDL.getDesginationValues();
            if (dt == null)
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
            }
            else
            {
                List<string> designations = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    designations.Add(dt.Rows[i][0].ToString());
                }
                cbxDesignation.DataSource = designations;
                cbxDesignation.Text = designation;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Guna.UI2.WinForms.Guna2TextBox[] TXTs = new Guna.UI2.WinForms.Guna2TextBox[5];
            ErrorProvider[] EPs = new ErrorProvider[5];
            TXTs[0] = txtFirstName;
            TXTs[1] = txtLastName;
            TXTs[2] = txtContact;
            TXTs[3] = txtEmail;
            TXTs[4] = txtSalary;
            EPs[0] = epFirstName;
            EPs[1] = epLastName;
            EPs[2] = epContact;
            EPs[3] = epEmail;
            EPs[4] = epSalary;
            if (ValidationsAndParsings.isEveryFieldSet(TXTs, EPs, 5)) // check all fields must be filled
            {
                if (ValidationsAndParsings.isNumber(txtSalary.Text))
                {
                    adv.FirstName = txtFirstName.Text;
                    adv.LastName = txtLastName.Text;
                    adv.Contact = txtContact.Text;
                    adv.Email = txtEmail.Text;
                    adv.DateOfBirth = adv.parseDate(tpDOB.Text);
                    adv.Gender = cbxGender.Text;
                    adv.Salary = txtSalary.Text;
                    adv.Designation = cbxDesignation.Text;
                    if (advisorDL.update(adv))
                    {
                        msgBox.Caption = Messages.CAPTION_SUCCESS;
                        msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                        msgBox.Text = Messages.SUCCESS_UPDATE;
                        msgBox.Show();
                        onSuccessUpdate?.Invoke(this, null);
                        this.Close();
                    }
                    else
                    {
                        msgBox.Caption = Messages.CAPTION_FAIL;
                        msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                        msgBox.Text = Messages.FAIL_UPDATE;
                        msgBox.Show();
                    }
                }
                else
                {
                    epSalary.SetError(txtSalary, Messages.NOT_NUMBER);
                }
            }
        }
    }
}