﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formManageStudents : Form
    {
        StudentDL studentDL;
        formSearchDialog formSearch;
        formUpdateStudentDialog formUpdate;
        public formManageStudents()
        {
            studentDL = new StudentDL();
            InitializeComponent();
        }

        private void formManageStudents_Load(object sender, EventArgs e)
        {
            DGV.MultiSelect = false;
            DGV.ScrollBars = ScrollBars.None;
            if (!studentDL.fetchRecords())
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
            }
            DGV.DataSource = studentDL.getList();
            changeDGVorder();
        }
        private void changeDGVorder()
        {
            DGV.AutoGenerateColumns = false;
            DGV.Columns["Id"].DisplayIndex = 0;
            DGV.Columns["RegistrationNumber"].DisplayIndex = 1;
            DGV.Columns["FirstName"].DisplayIndex = 2;
            DGV.Columns["LastName"].DisplayIndex = 3;
            DGV.Columns["Contact"].DisplayIndex = 4;
            DGV.Columns["Email"].DisplayIndex = 5;
            DGV.Columns["DateOfBirth"].DisplayIndex = 6;
            DGV.Columns["Gender"].DisplayIndex = 7;
            DGV.Columns["RegistrationNumber"].HeaderText = "Registration Number";
            DGV.Columns["FirstName"].HeaderText = "First Name";
            DGV.Columns["LastName"].HeaderText = "Last Name";
            DGV.Columns["DateOfBirth"].HeaderText = "Date Of Birth";
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            formSearch = new formSearchDialog("Enter ID");
            formSearch.searchClick += new EventHandler(onSearch);
            formSearch.ShowDialog();

        }

        private void onSearch(object sender, EventArgs e)
        {
            bool isFound = false;
            string id = formSearch.RegNo; //  form return reg no
            for (int i = 0; i < DGV.RowCount; i++)
            {
                // finding row that contain reg no
                if (DGV.Rows[i].Cells["Id"].Value.ToString() == id)
                {
                    DGV.ClearSelection();
                    DGV.Rows[i].Selected = true;
                    sbVertical.Value = i + 1;
                    isFound = true;
                    break;
                }
            }
            formSearch.isFound = isFound;

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (DGV.Rows.Count == 0)
                return;
            int rowIdx = DGV.SelectedRows[0].Index;
            string regNo = DGV.Rows[rowIdx].Cells["RegistrationNumber"].Value.ToString();
            string firstName = DGV.Rows[rowIdx].Cells["FirstName"].Value.ToString();
            string lastName = DGV.Rows[rowIdx].Cells["LastName"].Value.ToString();
            string contact = DGV.Rows[rowIdx].Cells["Contact"].Value.ToString();
            string email = DGV.Rows[rowIdx].Cells["Email"].Value.ToString();
            string dob = DGV.Rows[rowIdx].Cells["DateOfBirth"].Value.ToString();
            string gender = DGV.Rows[rowIdx].Cells["Gender"].Value.ToString();
            string id = DGV.Rows[rowIdx].Cells["Id"].Value.ToString();
            Student std = new Student(id, firstName, lastName, contact, email, dob
                                , gender, regNo);
            formUpdate = new formUpdateStudentDialog(std);
            formUpdate.onSuccessUpdate += new EventHandler(onSuccessUpdate);
            formUpdate.ShowDialog();
        }

        private void onSuccessUpdate(object sender, EventArgs e)
        {
            updateDGV();
        }
        private void updateDGV()
        {
            studentDL.fetchRecords();
            DGV.DataSource = studentDL.getList();
            changeDGVorder();
        }
        /*  private void btnDelete_Click(object sender, EventArgs e)
          {
              msgBox.Text = Messages.PROMPT_DELETE;
              msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Question;
              msgBox.Buttons = Guna.UI2.WinForms.MessageDialogButtons.YesNo;
              DialogResult result = msgBox.Show();
              if (result == DialogResult.Yes)
              {

                  if (studentDL.delete(DGV.SelectedRows[0].Cells["Id"].Value.ToString()))
                  {
                      msgBox.Caption = Messages.CAPTION_SUCCESS;
                      msgBox.Text = Messages.SUCCESS_DELETE;
                      msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                      msgBox.Buttons = Guna.UI2.WinForms.MessageDialogButtons.OK;
                      msgBox.Show();
                      updateDGV();

                  }
                  else
                  {
                      msgBox.Caption = Messages.CAPTION_FAIL;
                      msgBox.Text = Messages.FAIL_DELETE;
                      msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                      msgBox.Buttons = Guna.UI2.WinForms.MessageDialogButtons.OK;
                      msgBox.Show();
                  }
              }
          }
  */

        //----------------

        /*private void imgBtnPdf_Click(object sender, EventArgs e)
        {
            try
            {

                string path = "12";
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        path = fbd.SelectedPath;
                    }
                }

                path += "\\fypAllStudents.pdf";
                PdfWriter writer = new PdfWriter(path);
                iText.Kernel.Pdf.PdfDocument pdf = new iText.Kernel.Pdf.PdfDocument(writer);
                Document document = new Document(pdf);
                Paragraph header = new Paragraph("FYP Management System")
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(20).SetBold();
                document.Add(header);
                header = new Paragraph("List of All Students")
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(15);
                document.Add(header);
                LineSeparator ls = new LineSeparator(new SolidLine());
                document.Add(ls);
                header = new Paragraph("\n\n");
                document.Add(header);

                int dgvrowcount = DGV.Rows.Count;
                int dgvcolumncount = DGV.Columns.Count;

                // Set The Table like new float [] {15f, 15f, 15f, 15f, 15f }
                Table table = new Table(new float[] { 15f, 15f, 15f, 15f, 15f, 15f, 15f })
                    .SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER);
                table.SetAutoLayout();

                //table.SetWidth(iText.Layout.Properties.UnitValue.CreatePercentValue(100));

                // Print The DGV Header To Table Header
                for (int i = 0; i < dgvcolumncount; i++)
                {
                    Cell headerCells = new Cell()
                                  .SetBackgroundColor(iText.Kernel.Colors.ColorConstants.LIGHT_GRAY)
                                  .SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                    //headerCells.SetNextRenderer(new RoundedCornersCellRenderer(headerCells));
                    var gteCell = headerCells.Add(new Paragraph(DGV.Columns[i].HeaderText));
                    table.AddHeaderCell(gteCell);
                }
                // Print The DGV Cells To Table Cells
                for (int i = 0; i < dgvrowcount; i++)
                {
                    for (int c = 0; c < dgvcolumncount; c++)
                    {
                        Cell cells = new Cell()
                                  .SetBackgroundColor(iText.Kernel.Colors.ColorConstants.WHITE)
                                  .SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);

                        var gteCell = cells.Add(new Paragraph(DGV.Rows[i].Cells[DGV.Columns[c].HeaderText]
                            .Value.ToString()));
                        table.AddCell(gteCell);
                    }
                }
                document.Add(table);
                document.Close();
                MessageBox.Show("Document Has Been saved", "Success");
            }
            catch (Exception ee)
            {
                MessageBox.Show("Could Not Print");
            }
        }*/
    }
}
