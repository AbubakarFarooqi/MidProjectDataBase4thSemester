﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formAddStudent : Form
    {
        StudentDL StudentDL;
        public formAddStudent()
        {
            StudentDL = new StudentDL();
            InitializeComponent();
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        // This Function Will Check whther each field in the form is filled or not


        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            Guna.UI2.WinForms.Guna2TextBox[] TXTs = new Guna.UI2.WinForms.Guna2TextBox[5];
            ErrorProvider[] EPs = new ErrorProvider[5];
            TXTs[0] = txtFirstName;
            TXTs[1] = txtLastName;
            TXTs[2] = txtContact;
            TXTs[3] = txtEmail;
            TXTs[4] = txtRegNo;
            EPs[0] = epFirstName;
            EPs[1] = epLastName;
            EPs[2] = epContact;
            EPs[3] = epEmail;
            EPs[4] = epRegNo;
            if (ValidationsAndParsings.isEveryFieldSet(TXTs, EPs, 5)) // check all fields must be filled
            {
                Student student = new Student(txtFirstName.Text, txtLastName.Text, txtContact.Text, txtEmail.Text,
                                           tpDOB.Text, cbxGender.Text, txtRegNo.Text);

                if (StudentDL.insert(student))
                {
                    msgBox.Caption = Messages.CAPTION_SUCCESS;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                    msgBox.Text = Messages.SUCCESS_INSERT;
                    msgBox.Show();
                }
                else
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Text = Messages.FAIL_INSERT;
                    msgBox.Show();
                }

            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbxGender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
