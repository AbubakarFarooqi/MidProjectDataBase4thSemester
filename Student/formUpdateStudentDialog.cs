﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formUpdateStudentDialog : Form
    {
        Student student;
        StudentDL studentDL;
        public event EventHandler onSuccessUpdate;
        public formUpdateStudentDialog(Student student)
        {
            InitializeComponent();
            this.student = student;
            studentDL = new StudentDL();
            txtContact.Text = student.Contact;
            txtEmail.Text = student.Email;
            txtFirstName.Text = student.FirstName;
            txtLastName.Text = student.LastName;
            txtRegNo.Text = student.RegistrationNumber;
            cbxGender.Text = student.Gender;
            tpDOB.Text = student.DateOfBirth;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Guna.UI2.WinForms.Guna2TextBox[] TXTs = new Guna.UI2.WinForms.Guna2TextBox[5];
            ErrorProvider[] EPs = new ErrorProvider[5];
            TXTs[0] = txtFirstName;
            TXTs[1] = txtLastName;
            TXTs[2] = txtContact;
            TXTs[3] = txtEmail;
            TXTs[4] = txtRegNo;
            EPs[0] = epFirstName;
            EPs[1] = epLastName;
            EPs[2] = epContact;
            EPs[3] = epEmail;
            EPs[4] = epRegNo;
            if (ValidationsAndParsings.isEveryFieldSet(TXTs, EPs, 5)) // check all fields must be filled
            {
                //Student std = new Student(id, txtFirstName.Text, txtLastName.Text, txtContact.Text,
                //              txtEmail.Text, tpDOB.Text, cbxGender.Text, txtRegNo.Text);
                student.FirstName = txtFirstName.Text;
                student.LastName = txtLastName.Text;
                student.Contact = txtContact.Text;
                student.Email = txtEmail.Text;
                student.DateOfBirth = student.parseDate(tpDOB.Text);
                student.Gender = cbxGender.Text;
                student.RegistrationNumber = txtRegNo.Text;
                if (studentDL.update(student))
                {
                    msgBox.Caption = Messages.CAPTION_SUCCESS;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                    msgBox.Text = Messages.SUCCESS_UPDATE;
                    msgBox.Show();
                    onSuccessUpdate?.Invoke(this, null);
                    this.Close();
                }

                else
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Text = Messages.FAIL_UPDATE;
                    msgBox.Show();
                }
            }
        }
    }
}
