﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formManageEvaluations : Form
    {
        EvaluationDL evaluationDL;
        public formManageEvaluations()
        {
            evaluationDL = new EvaluationDL();
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formManageEvaluations_Load(object sender, EventArgs e)
        {
            this.btnUpdate.Hide();
            DGV.MultiSelect = false;
            if (!evaluationDL.fetchRecords())
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
            }
            DGV.DataSource = evaluationDL.getList();
            changeDGVorder();
        }
        private void changeDGVorder()
        {
            DGV.AutoGenerateColumns = false;
            DGV.Columns["Id"].DisplayIndex = 0;
            DGV.Columns["Name"].DisplayIndex = 1;
            DGV.Columns["TotalMarks"].DisplayIndex = 2;
            DGV.Columns["TotalWeightage"].DisplayIndex = 3;

            DGV.Columns["TotalWeightage"].HeaderText = "Total Weightage";
            DGV.Columns["TotalMarks"].HeaderText = "Total Marks";
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (DGV.RowCount == 0)
                return;
            int rowIdx = DGV.SelectedRows[0].Index;
            List<Evaluation> lst = (List<Evaluation>)DGV.DataSource;
            Evaluation evaluation = lst[rowIdx];
            txtName.Text = evaluation.Name;
            txtTotalMarks.Text = evaluation.Totalmarks;
            txtTotalWeightage.Text = evaluation.TotalWeightage;
            btnUpdate.Visible = true;
            DGV.Enabled = false;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Guna.UI2.WinForms.Guna2TextBox[] TXTs = new Guna.UI2.WinForms.Guna2TextBox[3];
            ErrorProvider[] EPs = new ErrorProvider[3];
            TXTs[0] = txtName;
            TXTs[1] = txtTotalMarks;
            TXTs[2] = txtTotalWeightage;
            EPs[0] = epName;
            EPs[1] = epTotalMarks;
            EPs[2] = epTotalWeightage;
            if (ValidationsAndParsings.isEveryFieldSet(TXTs, EPs, 3))
            {
                if (ValidationsAndParsings.isNumber(txtTotalMarks.Text))
                {
                    if (ValidationsAndParsings.isNumber(txtTotalWeightage.Text))
                    {
                        string id = ((List<Evaluation>)DGV.DataSource)[DGV.SelectedRows[0].Index].Id;
                        Evaluation evaluation = new Evaluation(id, txtName.Text, txtTotalMarks.Text, txtTotalWeightage.Text);
                        if (evaluationDL.isAcceptableWeightageUpdate(id, txtTotalWeightage.Text))
                        {
                            if (evaluationDL.update(evaluation))
                            {
                                msgBox.Caption = Messages.CAPTION_SUCCESS;
                                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                                msgBox.Text = Messages.SUCCESS_UPDATE;
                                msgBox.Show();
                                this.Close();
                            }
                            else
                            {
                                msgBox.Caption = Messages.CAPTION_FAIL;
                                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                                msgBox.Text = Messages.FAIL_UPDATE;
                                msgBox.Show();
                                this.Close();
                            }
                        }
                        else
                        {
                            msgBox.Caption = Messages.CAPTION_FAIL;
                            msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                            msgBox.Text = Messages.NOT_ACCEPTABLE_WEIGHTAGE;
                            msgBox.Show();
                        }
                    }
                    else
                    {
                        epTotalWeightage.SetError(txtTotalWeightage, Messages.NOT_NUMBER);
                    }
                }
                else
                {
                    epTotalMarks.SetError(txtTotalMarks, Messages.NOT_NUMBER);
                }
            }


        }
    }

}
