﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formCreateEvaluation : Form
    {
        EvaluationDL evaluationDL;
        public formCreateEvaluation()
        {
            evaluationDL = new EvaluationDL();
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formCreateEvaluation_Load(object sender, EventArgs e)
        {

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            Guna.UI2.WinForms.Guna2TextBox[] TXTs = new Guna.UI2.WinForms.Guna2TextBox[3];
            ErrorProvider[] EPs = new ErrorProvider[3];
            TXTs[0] = txtName;
            TXTs[1] = txtTotalMarks;
            TXTs[2] = txtTotalWeightage;
            EPs[0] = epName;
            EPs[1] = epTotalMarks;
            EPs[2] = epTotalWeightage;
            if (ValidationsAndParsings.isEveryFieldSet(TXTs, EPs, 3))
            {
                if (ValidationsAndParsings.isNumber(txtTotalMarks.Text) && int.Parse(txtTotalMarks.Text) > 0)
                {
                    if (ValidationsAndParsings.isNumber(txtTotalWeightage.Text) && int.Parse(txtTotalWeightage.Text) > 0)
                    {
                        if (evaluationDL.isAcceptableWeightage(txtTotalWeightage.Text))
                        {
                            Evaluation evaluation = new Evaluation(txtName.Text, txtTotalMarks.Text, txtTotalWeightage.Text);
                            if (evaluationDL.insert(evaluation))
                            {
                                msgBox.Caption = Messages.CAPTION_SUCCESS;
                                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                                msgBox.Text = Messages.SUCCESS_INSERT;
                                msgBox.Show();
                            }
                            else
                            {
                                msgBox.Caption = Messages.CAPTION_FAIL;
                                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                                msgBox.Text = Messages.FAIL_INSERT;
                                msgBox.Show();
                            }
                        }
                        else
                        {
                            msgBox.Caption = Messages.CAPTION_FAIL;
                            msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                            msgBox.Text = Messages.NOT_ACCEPTABLE_WEIGHTAGE;
                            msgBox.Show();
                        }
                    }
                    else
                    {
                        epTotalWeightage.SetError(txtTotalWeightage, Messages.NOT_NUMBER);
                    }
                }
                else
                {
                    epTotalMarks.SetError(txtTotalMarks, Messages.NOT_NUMBER);
                }
            }


        }

        private void txtTotalWeightage_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
