﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidProject.BL
{
    public class Group
    {
        string id;
        string created_on;

        /// <summary>
        /// conctructs a group from App Layer
        /// </summary>
        /// <param name="created_on"></param>
        public Group(string created_on)
        {
            this.created_on = created_on;
        }

        /// <summary>
        /// constructs group from database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="created_on"></param>
        public Group(string id, string created_on)
        {
            this.id = id;
            this.created_on = created_on;
        }
        public string Id { get => id; set => id = value; }
        public string Created_on { get => created_on; set => created_on = value; }
    }
}
