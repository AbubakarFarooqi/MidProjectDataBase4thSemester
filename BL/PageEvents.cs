﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp;

namespace MidProject.BL
{
    public class PageEvents : PdfPageEventHelper
    {
        // This is the contentbyte object of the writer
        PdfContentByte cb;

        // we will put the final number of pages in a template
        PdfTemplate headerTemplate, footerTemplate;

        // this is the BaseFont we are going to use for the header / footer
        BaseFont bf = null;

        // This keeps track of the creation time
        DateTime PrintTime = DateTime.Now;

        #region Fields
        private string _header;
        #endregion

        #region Properties
        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }
        #endregion

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                PrintTime = DateTime.Now;
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                headerTemplate = cb.CreateTemplate(100, 100);
                footerTemplate = cb.CreateTemplate(50, 50);
            }
            catch (DocumentException de)
            {
            }
            catch (System.IO.IOException ioe)
            {
            }
        }

        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            if (writer.PageNumber == 1)
                return;
            base.OnEndPage(writer, document);
            iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            Phrase p1Header = new Phrase("   FYP Management System", baseFontBig);

            //Create PdfTable object

            iTextSharp.text.pdf.PdfPTable pdfTab = new iTextSharp.text.pdf.PdfPTable(3);
            //We will have to create separate cells to include image logo and 2 separate strings
            //Row 1
            System.Drawing.Image image = Properties.Resources.uet_lahore_logo_5914237A23_seeklogo_com;
            iTextSharp.text.Image image1 = iTextSharp.text.Image.GetInstance(image, System.Drawing.Imaging.ImageFormat.Png);
            image1.Alignment = Element.ALIGN_LEFT;
            image1.ScaleAbsolute(40f, 40f);

            System.Drawing.Image imagec = Properties.Resources.cs_final_logo_ai_with_color_trnasparent;
            iTextSharp.text.Image image1c = iTextSharp.text.Image.GetInstance(imagec, System.Drawing.Imaging.ImageFormat.Png);
            image1c.Alignment = Element.ALIGN_RIGHT;
            image1c.ScaleAbsolute(40f, 40f);

            PdfPCell pdfCell1 = new PdfPCell(image1);
            PdfPCell pdfCell2 = new PdfPCell(p1Header);
            PdfPCell pdfCell3 = new PdfPCell(image1c);
            String text = (writer.PageNumber - 1).ToString();

            /*  //Add paging to header
              {
                  cb.BeginText();
                  cb.SetFontAndSize(bf, 12);
                  cb.SetTextMatrix(document.PageSize.GetRight(200), document.PageSize.GetTop(45));
                  cb.ShowText(text);
                  cb.EndText();
                  float len = bf.GetWidthPoint(text, 12);
                  //Adds "12" in Page 1 of 12
                  cb.AddTemplate(headerTemplate, document.PageSize.GetRight(200) + len, document.PageSize.GetTop(45));
              }*/
            //Add paging to footer
            {
                cb.BeginText();
                cb.SetFontAndSize(bf, 12);
                cb.SetTextMatrix(document.PageSize.GetRight(180), document.PageSize.GetBottom(30));
                cb.ShowText(text);
                cb.EndText();
                float len = bf.GetWidthPoint(text, 12);
                cb.AddTemplate(footerTemplate, document.PageSize.GetRight(180) + len, document.PageSize.GetBottom(30));
            }

            //Row 2
            /*PdfPCell pdfCell4 = new PdfPCell(new Phrase("Sub Header Description", baseFontNormal));

            //Row 3 
            PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
            PdfPCell pdfCell6 = new PdfPCell();
            PdfPCell pdfCell7 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));*/

            //set the alignment of all three cells and set border to 0
            pdfCell1.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell3.HorizontalAlignment = Element.ALIGN_RIGHT;
            /* pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
             pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
             pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
             pdfCell7.HorizontalAlignment = Element.ALIGN_CENTER;*/

            pdfCell1.VerticalAlignment = Element.ALIGN_TOP;
            pdfCell2.VerticalAlignment = Element.ALIGN_TOP;
            pdfCell3.VerticalAlignment = Element.ALIGN_TOP;
            /*   pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
               pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
               pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
               pdfCell7.VerticalAlignment = Element.ALIGN_MIDDLE;*/
            /*
                        pdfCell4.Colspan = 3;*/

            pdfCell1.Border = 0;
            pdfCell2.Border = 0;
            pdfCell3.Border = 0;


            /* pdfCell4.Border = 0;
             pdfCell5.Border = 0;
             pdfCell6.Border = 0;
             pdfCell7.Border = 0;*/
            //add all three cells into PdfTable
            pdfTab.AddCell(pdfCell1);
            pdfTab.AddCell(pdfCell2);
            pdfTab.AddCell(pdfCell3);

            /*  pdfTab.AddCell(pdfCell4);
              pdfTab.AddCell(pdfCell5);
              pdfTab.AddCell(pdfCell6);
              pdfTab.AddCell(pdfCell7);*/

            pdfTab.TotalWidth = document.PageSize.Width - 100f;
            pdfTab.WidthPercentage = 100;
            pdfTab.HorizontalAlignment = Element.ALIGN_CENTER;
            float width = document.PageSize.Width;
            float[] col = { 30f, 500f, 30f };
            pdfTab.SetTotalWidth(col);
            //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
            //first param is start row. -1 indicates there is no end row and all the rows to be included to write
            //Third and fourth param is x and y position to start writing
            pdfTab.WriteSelectedRows(0, -1, 15, document.PageSize.Height - 30, writer.DirectContent);
            //set pdfContent value
            //Move the pointer and draw line to separate header section from rest of page
            cb.MoveTo(8, document.PageSize.Height - 80);
            cb.LineTo(document.PageSize.Width - 8, document.PageSize.Height - 80);
            cb.Stroke();

            //Move the pointer and draw line to separate footer section from rest of page
            cb.MoveTo(40, document.PageSize.GetBottom(50));
            cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
            cb.Stroke();
        }

        /*public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            headerTemplate.BeginText();
            headerTemplate.SetFontAndSize(bf, 12);
            headerTemplate.SetTextMatrix(0, 0);
            headerTemplate.ShowText((writer.PageNumber - 1).ToString());
            headerTemplate.EndText();

            footerTemplate.BeginText();
            footerTemplate.SetFontAndSize(bf, 12);
            footerTemplate.SetTextMatrix(0, 0);
            footerTemplate.ShowText((writer.PageNumber - 1).ToString());
            footerTemplate.EndText();
        }*/
    }
}
