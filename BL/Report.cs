﻿using MidProject.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace MidProject.BL
{
    public class Report
    {
        PdfPageEventHelper pageEventHelper;
        public FileStream getStream(SaveFileDialog save)
        {

            if (File.Exists(save.FileName))

            {
                try

                {

                    File.Delete(save.FileName);

                }

                catch (Exception ex)

                {
                    return null;
                }

            }
            FileStream fileStream = new FileStream(save.FileName, FileMode.Create);
            return fileStream;
        }
        private Document getDocument(FileStream fileStream)
        {
            // try
            //     {
            iTextSharp.text.Document document = new Document(PageSize.A4, 8f, 8f, 100f, 55f);
            //document.Open();
            PdfWriter writer = PdfWriter.GetInstance(document, fileStream);
            writer.PageEvent = new PageEvents();
            document.Open();
            return document;
            //  }
            // catch (Exception e)
            //  {
            //     return null;
            // }
        }
        private void addfrontPage(Document document)
        {
            document.Add(new Paragraph("\n\n\n"));

            //   document.AddTitle("FInal Year Project Management System by Khawaja Saad Akbar (2021-CS-172)");

            iTextSharp.text.Font font = FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 20, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font font1 = FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 18, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font font112 = FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 16, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

            iTextSharp.text.Paragraph elements = new iTextSharp.text.Paragraph("FYP MANAGEMENT SYSTEM\n", font);
            elements.Alignment = Element.ALIGN_CENTER;
            document.Add(elements);


            document.Add(new Paragraph("\n\n\n"));

            System.Drawing.Image image = Properties.Resources.uet_lahore_logo_5914237A23_seeklogo_com;
            iTextSharp.text.Image image1 = iTextSharp.text.Image.GetInstance(image, System.Drawing.Imaging.ImageFormat.Png);
            image1.Alignment = Element.ALIGN_CENTER;
            image1.ScaleAbsolute(150f, 150f);
            document.Add(image1);

            document.Add(new Paragraph("\n\n\n"));

            Paragraph elements11 = new Paragraph("Generated by:\n\n", font1);
            elements11.Alignment = Element.ALIGN_CENTER;
            document.Add(elements11);



            Paragraph elements111 = new Paragraph("FYP Management Office", font112);
            elements111.Alignment = Element.ALIGN_CENTER;
            document.Add(elements111);

            document.Add(new Paragraph("\n\n\n"));

            Paragraph elements112 = new Paragraph("Supervised By:\n\n", font1);
            elements112.Alignment = Element.ALIGN_CENTER;
            document.Add(elements112);

            Paragraph elements1112 = new Paragraph("Mr. Samyan Qayyum Wahla and Nauman Babar\n", font112);
            elements1112.Alignment = Element.ALIGN_CENTER;
            document.Add(elements1112);

            document.Add(new Paragraph("\n\n"));
            document.Add(new Paragraph("\n\n"));

            Paragraph css = new Paragraph("Department of Computer Science\n", font112);
            css.Alignment = Element.ALIGN_CENTER;
            document.Add(css);

            iTextSharp.text.Paragraph element = new iTextSharp.text.Paragraph("University of Engineering and Technology", font);
            element.Alignment = Element.ALIGN_CENTER;
            document.Add(element);

            document.NewPage();
        }
        private iTextSharp.text.pdf.PdfPTable dataTableToPdfPTable(DataTable dt, bool color = false)
        {
            iTextSharp.text.pdf.PdfPTable table = new iTextSharp.text.pdf.PdfPTable(dt.Columns.Count);
            /*After that, we need to create table cells for each of the columns we specified. 
             * To create table cells we use PdfPCell class provided by the iTextSharp.*/
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                PdfPCell cell = new PdfPCell(); //create object from the pdfpcell class
                cell.BackgroundColor = BaseColor.LIGHT_GRAY; //set color of cells to gray
                cell.AddElement(new Chunk(dt.Columns[j].ColumnName,
                                new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                                12, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK)));
                table.AddCell(cell);

            }

            //add actual rows from grid to table
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                table.WidthPercentage = 100; //set width of the table
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    if (color)
                    {
                        if (dt.Rows[i][k].ToString() != null && dt.Rows[i][k].ToString() == "Marked")
                        {
                            iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.GREEN);
                            // get the value of   each cell in the dataTable tblemp
                            table.AddCell(new Phrase(dt.Rows[i][k].ToString(), baseFontBig));
                        }
                        else if (dt.Rows[i][k].ToString() != null && dt.Rows[i][k].ToString() == "Not Marked")
                        {
                            iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.RED);
                            // get the value of   each cell in the dataTable tblemp
                            table.AddCell(new Phrase(dt.Rows[i][k].ToString(), baseFontBig));
                        }
                        else
                        {
                            if (dt.Rows[i][k].ToString() != null)
                                // get the value of   each cell in the dataTable tblemp
                                table.AddCell(new Phrase(dt.Rows[i][k].ToString()));
                        }
                    }
                    else
                    {
                        if (dt.Rows[i][k].ToString() != null)
                            // get the value of   each cell in the dataTable tblemp
                            table.AddCell(new Phrase(dt.Rows[i][k].ToString()));
                    }
                }
            }
            return table;
        }
        private void addHeading(string text, Document document)
        {
            Paragraph header = new Paragraph(text + "\n\n", FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 15, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK));
            header.Alignment = Element.ALIGN_CENTER;
            document.Add(header);
        }

        private void addParagraph(string text, Document document)
        {
            Paragraph header = new Paragraph(text + "\n\n", FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 12, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK));
            header.Alignment = Element.ALIGN_LEFT;
            document.Add(header);
        }
        public bool generateListOfAllStudents(FileStream fileStream)
        {
            DataTable studentTable = new ReportDL().getAllStudents();
            if (studentTable == null)
                return false;
            Document document = getDocument(fileStream);
            if (document == null)
                return false;
            addfrontPage(document);
            addHeading("List of All Students", document);
            iTextSharp.text.pdf.PdfPTable pdfPTable = dataTableToPdfPTable(studentTable);
            document.Add(pdfPTable);
            document.Close();
            fileStream.Close();
            return true;
        }

        public bool generateListOfAllAdvisors(FileStream fileStream)
        {
            DataTable advisorTable = new ReportDL().getAllAdvisors();
            if (advisorTable == null)
                return false;
            Document document = getDocument(fileStream);
            if (document == null)
                return false;
            addfrontPage(document);
            addHeading("List of All Advisors", document);
            iTextSharp.text.pdf.PdfPTable pdfPTable = dataTableToPdfPTable(advisorTable);
            document.Add(pdfPTable);
            document.Close();
            fileStream.Close();
            return true;
        }
        public bool generateNonGroupProjects(FileStream fileStream)
        {
            DataTable nonGroupProjectTable = new ReportDL().getNonGroupProject();
            if (nonGroupProjectTable == null)
                return false;
            Document document = getDocument(fileStream);
            if (document == null)
                return false;
            addfrontPage(document);
            addHeading("List of Non Group Projects", document);
            addParagraph("These are the projects which are not assigned to any group", document);
            iTextSharp.text.pdf.PdfPTable pdfPTable = dataTableToPdfPTable(nonGroupProjectTable);
            document.Add(pdfPTable);
            document.Close();
            fileStream.Close();
            return true;
        }
        public bool generateNonGroupStudents(FileStream fileStream)
        {
            DataTable nonGroupstudentTable = new ReportDL().getNonGroupStudents();
            if (nonGroupstudentTable == null)
                return false;
            Document document = getDocument(fileStream);
            if (document == null)
                return false;
            addfrontPage(document);
            addHeading("List of Non Group Students", document);
            addParagraph("These are the students which are not in any group", document);
            iTextSharp.text.pdf.PdfPTable pdfPTable = dataTableToPdfPTable(nonGroupstudentTable);
            document.Add(pdfPTable);
            document.Close();
            fileStream.Close();
            return true;
        }

        public bool generateNonProjectAdvisors(FileStream fileStream)
        {
            DataTable nonProjectAdvisorTable = new ReportDL().getNonProjectAdvisors();
            if (nonProjectAdvisorTable == null)
                return false;
            Document document = getDocument(fileStream);
            if (document == null)
                return false;
            addfrontPage(document);
            addHeading("List of Non Project Advisors", document);
            addParagraph("These are the advisors which are not in any project", document);
            iTextSharp.text.pdf.PdfPTable pdfPTable = dataTableToPdfPTable(nonProjectAdvisorTable);
            document.Add(pdfPTable);
            document.Close();
            fileStream.Close();
            return true;
        }
        public bool generateEvalutionsStatus(FileStream fileStream)
        {
            DataTable temp = new ReportDL().getEvaluationsNames();
            DataTable dt = new DataTable();
            DataColumn grp = new DataColumn("GroupId", typeof(string));
            dt.Columns.Add(grp);
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                DataColumn clm = new DataColumn(temp.Rows[i][0].ToString(), typeof(string));
                dt.Columns.Add(clm);
            }
            DataTable grpIds = new ReportDL().getGroupIds();
            for (int i = 0; i < grpIds.Rows.Count; i++)
            {
                object[] obj = new object[temp.Rows.Count + 1];
                obj[0] = grpIds.Rows[i][0].ToString();
                for (int j = 0; j < temp.Rows.Count; j++)
                {
                    if ((new ReportDL().isEvaluated(grpIds.Rows[i][0].ToString(), temp.Rows[j][1].ToString())))
                    {
                        obj[j + 1] = "Marked";
                    }
                    else
                    {
                        obj[j + 1] = "Not Marked";
                    }
                }
                dt.Rows.Add(obj);
            }



            Document document = getDocument(fileStream);
            if (document == null)
                return false;
            addfrontPage(document);
            addHeading("List of Non Project Advisors", document);
            addParagraph("These are the advisors which are not in any project", document);
            iTextSharp.text.pdf.PdfPTable pdfPTable = dataTableToPdfPTable(dt, true);
            document.Add(pdfPTable);
            document.Close();
            fileStream.Close();
            return true;
        }
        public bool generateGroupWiseEvalutionsResult(FileStream fileStream)
        {
            DataTable grpIds = new ReportDL().getGroupIds();
            DataTable evaluationsName = new ReportDL().getEvaluationsNames();
            iTextSharp.text.pdf.PdfPTable mainTable = new iTextSharp.text.pdf.PdfPTable(3);
            PdfPCell cell = new PdfPCell(); //create object from the pdfpcell class
            cell.BackgroundColor = BaseColor.LIGHT_GRAY; //set color of cells to gray
            cell.AddElement(new Chunk(grpIds.Columns[0].ColumnName,
                                         new iTextSharp.text.Font(iTextSharp.text.Font
                                         .FontFamily.HELVETICA, 12,
                                         iTextSharp.text.Font.BOLD,
                                         iTextSharp.text.BaseColor.BLACK)));
            mainTable.SetWidths(new float[] { 5f, 25f, 70f });
            mainTable.AddCell(cell);
            cell = new PdfPCell();
            cell.BackgroundColor = BaseColor.LIGHT_GRAY; //set color of cells to gray
            cell.AddElement(new Chunk("Student Names",
                                         new iTextSharp.text.Font(iTextSharp.text.Font
                                         .FontFamily.HELVETICA, 12,
                                         iTextSharp.text.Font.BOLD,
                                         iTextSharp.text.BaseColor.BLACK)));
            mainTable.AddCell(cell);

            cell = new PdfPCell();
            cell.BackgroundColor = BaseColor.LIGHT_GRAY; //set color of cells to gray
            cell.AddElement(new Chunk("Evluations",
                                         new iTextSharp.text.Font(iTextSharp.text.Font
                                         .FontFamily.HELVETICA, 12,
                                         iTextSharp.text.Font.BOLD,
                                         iTextSharp.text.BaseColor.BLACK)));
            mainTable.AddCell(cell);


            //........................Main table Column headers done above


            iTextSharp.text.pdf.PdfPTable evTab = new iTextSharp.text.pdf.PdfPTable(evaluationsName.Rows.Count);
            DataTable evDat = new DataTable();
            for (int l = 0; l < evaluationsName.Rows.Count; l++)
            {
                DataColumn clm = new DataColumn(evaluationsName.Rows[l][0].ToString(), typeof(string));
                evDat.Columns.Add(clm);
            }
            int size = 0;
            for (int j = 0; j < evDat.Columns.Count; j++)
            {
                cell = new PdfPCell(); //create object from the pdfpcell class
                cell.BackgroundColor = BaseColor.LIGHT_GRAY; //set color of cells to gray
                cell.AddElement(new Chunk(evDat.Columns[j].ColumnName,
                                new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                                12, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK)));
                evTab.AddCell(cell);
                size = j;

            }
            float[] wid = new float[size + 1];
            for (int i = 0; i < size + 1; i++)
            {
                wid[i] = 12f;
            }
            evTab.SetWidths(wid);


            mainTable.AddCell(new Phrase(""));
            mainTable.AddCell(new Phrase(""));
            mainTable.AddCell(evTab);


            //...................................Evalutions Header done above.
            mainTable.WidthPercentage = 100;

            for (int i = 0; i < grpIds.Rows.Count; i++)
            {
                string groupId = grpIds.Rows[i][0].ToString();
                DataTable stdNames = new ReportDL().getGroupstudent(groupId);

                iTextSharp.text.pdf.PdfPTable table1 = new iTextSharp.text.pdf.PdfPTable(1);
                /* PdfPCell cell1 = new PdfPCell(); //create object from the pdfpcell class
                 cell1.BackgroundColor = BaseColor.LIGHT_GRAY; //set color of cells to gray
                 cell1.AddElement(new Chunk("",
                                              new iTextSharp.text.Font(iTextSharp.text.Font
                                              .FontFamily.HELVETICA, 12,
                                              iTextSharp.text.Font.BOLD,
                                              iTextSharp.text.BaseColor.BLACK)));
                 table1.AddCell(cell1);*/



                for (int k = 0; k < stdNames.Rows.Count; k++)
                {
                    if (stdNames.Rows[k][0].ToString() != null)
                    {
                        iTextSharp.text.Font baseFontBig1 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
                        // get the value of   each cell in the dataTable tblemp
                        table1.AddCell(new Phrase(stdNames.Rows[k][0].ToString(), baseFontBig1));
                    }
                }

                //..................................................................................................................

                // start adding marks
                /*iTextSharp.text.pdf.PdfPTable evMarksTab = new iTextSharp.text.pdf.PdfPTable(evaluationsName.Rows.Count);
                DataTable evMarksDat = new DataTable();



                for (int ii = 0; ii < evaluationsName.Rows.Count; i++)
                {
                    DataColumn clm = new DataColumn(evaluationsName.Rows[ii][0].ToString(), typeof(string));
                    evMarksDat.Columns.Add(clm);
                }



                object[] obj = new object[evaluationsName.Rows.Count];

                for (int j = 0; j < evaluationsName.Rows.Count; j++)
                {
                    string marks = new ReportDL().getEvaluatedmarks(groupId, evaluationsName.Rows[j][1].ToString());
                    obj[j] = marks;
                }
                evMarksDat.Rows.Add(obj);
*/


                ////////////////////////////////////////////////////////
                /*iTextSharp.text.pdf.PdfPTable evMarksTab = new iTextSharp.text.pdf.PdfPTable(evaluationsName.Rows.Count);
                DataTable evMarksDat = new DataTable();*/
                /* for (int l = 0; l < evaluationsName.Rows.Count; l++)
                 {
                     DataColumn clm = new DataColumn(evaluationsName.Rows[l][0].ToString(), typeof(string));
                     evMarksDat.Columns.Add(clm);
                 }*/
                /*     size = 0;
                     for (int j = 0; j < evMarksDat.Columns.Count; j++)
                     {
                         cell = new PdfPCell(); //create object from the pdfpcell class
                         cell.BackgroundColor = BaseColor.LIGHT_GRAY; //set color of cells to gray
                         cell.AddElement(new Chunk(evMarksDat.Rows[0].ItemArray[j].ToString(),
                                         new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                                         12, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK)));
                         evMarksTab.AddCell(cell);
                         size = j;

                     }
                     wid = new float[size + 1];
                     for (int ii = 0; ii < size + 1; ii++)
                     {
                         wid[ii] = 12f;
                     }
                     evMarksTab.SetWidths(wid);*/
                ///////////////////////////////////////////////////////////////////////////




                /*
                                iTextSharp.text.pdf.PdfPTable evTab = new iTextSharp.text.pdf.PdfPTable(evaluationsName.Rows.Count);
                                DataTable evDat = new DataTable();
                                for (int l = 0; l < evaluationsName.Rows.Count; l++)
                                {
                                    DataColumn clm = new DataColumn(evaluationsName.Rows[l][0].ToString(), typeof(string));
                                    evDat.Columns.Add(clm);
                                }
                                int size = 0;*/
                /*for (int j = 0; j < evDat.Columns.Count; j++)
                {
                    cell = new PdfPCell(); //create object from the pdfpcell class
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY; //set color of cells to gray
                    cell.AddElement(new Chunk(evDat.Columns[j].ColumnName,
                                    new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                                    12, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK)));
                    evTab.AddCell(cell);
                    size = j;

                }*/
                /*   float[]*/
                /*                wid = new float[size + 1];
                                for (int ii = 0; ii < size + 1; ii++)
                                {
                                    wid[ii] = 12f;
                                }
                                evTab.SetWidths(wid);*/




                evTab = new iTextSharp.text.pdf.PdfPTable(evaluationsName.Rows.Count);
                evDat = new DataTable();
                for (int j = 0; j < evaluationsName.Rows.Count; j++)
                {
                    string marks = new ReportDL().getEvaluatedmarks(groupId, evaluationsName.Rows[j][1].ToString());
                    if (marks != "Not Marked")
                    {
                        cell = new PdfPCell(); //create object from the pdfpcell class
                        cell.BackgroundColor = BaseColor.WHITE; //set color of cells to gray
                        cell.AddElement(new Chunk(marks,
                                        new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                                        12, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK)));
                        evTab.AddCell(cell);
                    }
                    else
                    {
                        cell = new PdfPCell(); //create object from the pdfpcell class
                        cell.BackgroundColor = BaseColor.WHITE; //set color of cells to gray
                        cell.AddElement(new Chunk(marks,
                                        new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                                        12, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.RED)));
                        evTab.AddCell(cell);
                    }
                    size = j;

                }
                wid = new float[size + 1];
                for (int ii = 0; ii < size + 1; ii++)
                {
                    wid[ii] = 12f;
                }
                evTab.SetWidths(wid);




                iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
                mainTable.AddCell(new Phrase(groupId, baseFontBig));
                mainTable.AddCell(table1);
                mainTable.AddCell(evTab);
            }
            // iTextSharp.text.pdf.PdfPTable studentNameTab = new iTextSharp.text.pdf.PdfPTable();










            /*
                        DataTable dt = new DataTable();
                        DataColumn grp = new DataColumn("GroupId", typeof(string));
                        dt.Columns.Add(grp);
                        for (int i = 0; i < temp.Rows.Count; i++)
                        {
                            DataColumn clm = new DataColumn(temp.Rows[i][0].ToString(), typeof(string));
                            dt.Columns.Add(clm);
                        }
                        for (int i = 0; i < grpIds.Rows.Count; i++)
                        {
                            object[] obj = new object[temp.Rows.Count + 1];
                            obj[0] = grpIds.Rows[i][0].ToString();
                            for (int j = 0; j < temp.Rows.Count; j++)
                            {
                                if ((new ReportDL().isEvaluated(grpIds.Rows[i][0].ToString(), temp.Rows[j][1].ToString())))
                                {
                                    obj[j + 1] = "Marked";
                                }
                                else
                                {
                                    obj[j + 1] = "Not Marked";
                                }
                            }
                            dt.Rows.Add(obj);
                        }

            */

            Document document = getDocument(fileStream);
            if (document == null)
                return false;
            addfrontPage(document);
            addHeading("List of Non Project Advisors", document);
            addParagraph("These are the advisors which are not in any project", document);
            // iTextSharp.text.pdf.PdfPTable pdfPTable = dataTableToPdfPTable(dt, true);
            document.Add(mainTable);
            document.Close();
            fileStream.Close();
            return true;
        }
        /*    private void GenerateReport_Click(object sender, EventArgs e)
            {



                //TABLE 9:-
                PdfPTable ALLEVALUATIONS = getTableFromDataGridView(AllEvaluations);

                DataGridView TopGroup = new DataGridView();
                TopGroup.Visible = false;
                TopGroup.AllowUserToAddRows = false;
                TopGroup.ReadOnly = true;
                this.Controls.Add((TopGroup));
                TopGroup.DataSource = getTopGroup();

                //TABLE 10:-
                PdfPTable TOPGROUP = getTableFromDataGridView(TopGroup);




                //__________________________________________________________________________DOCUMENT WRITING__________________________________________________________________________//

                iText.text.Font MainHeading = FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 16, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

                SaveFileDialog save = new SaveFileDialog();

                save.Filter = "PDF (*.pdf)|*.pdf";

                save.FileName = "Report.pdf";

                bool ErrorMessage = false;

                if (save.ShowDialog() == DialogResult.OK)

                {

                    if (File.Exists(save.FileName))

                    {

                        try

                        {

                            File.Delete(save.FileName);

                        }

                        catch (Exception ex)

                        {

                            ErrorMessage = true;

                            MessageBox.Show("Unable to wride data in disk" + ex.Message);

                        }

                    }

                    if (!ErrorMessage)

                    {

                        try

                        {

                            using (FileStream fileStream = new FileStream(save.FileName, FileMode.Create))

                            {

                                iTextSharp.text.Document document = new Document(PageSize.A4, 8f, 16f, 16f, 8f);

                                document.Open();

                                PdfWriter.GetInstance(document, fileStream);

                                WriteFirstPage(document);

                                Paragraph first = new Paragraph("LIST OF STUDENTS\n\n", MainHeading);
                                first.Alignment = Element.ALIGN_CENTER;
                                document.Add(first);

                                document.Add(ALLSTUDENTS);
                                document.NewPage();

                                Paragraph second = new Paragraph("LIST OF ALL PROJECTS AND THEIR DESCRIPTION\n\n", MainHeading);
                                second.Alignment = Element.ALIGN_CENTER;

                                document.Add(second);
                                document.Add(ALLPROJECTS);
                                document.NewPage();

                                Paragraph third = new Paragraph("LIST OF GROUPS WITH THEIR STUDENTS AND PROJECT\n\n", MainHeading);
                                third.Alignment = Element.ALIGN_CENTER;
                                document.Add(third);
                                document.Add(GROUPSINFORMATION);
                                document.NewPage();


                                Paragraph fourth = new Paragraph("LIST OF ALL ADVISORS\n\n", MainHeading);
                                fourth.Alignment = Element.ALIGN_CENTER;
                                document.Add(fourth);
                                document.Add(ALLADVISORS);
                                document.NewPage();

                                Paragraph fifth = new Paragraph("LIST OF ALL PROJECTS WITH THEIR RESPECTIVE ADVISORS\n\n", MainHeading);
                                fifth.Alignment = Element.ALIGN_CENTER;
                                document.Add(fifth);
                                document.Add(PROJECTSBOARD);
                                document.NewPage();

                                Paragraph tenth = new Paragraph("TOTAL EVALUATIONS\n\n", MainHeading);
                                tenth.Alignment = Element.ALIGN_CENTER;
                                document.Add(tenth);
                                document.Add(ALLEVALUATIONS);
                                document.NewPage();

                                Paragraph ninth = new Paragraph("MARKS OF STUDENTS AGAINST EACH EVALUATION\n\n", MainHeading);
                                ninth.Alignment = Element.ALIGN_CENTER;
                                document.Add(ninth);
                                document.Add(MARKSHEET);
                                document.NewPage();

                                Paragraph eleventh = new Paragraph("TOTAL MARKS OF EACH STUDENT\n\n", MainHeading);
                                eleventh.Alignment = Element.ALIGN_CENTER;
                                document.Add(eleventh);
                                document.Add(TOTALMARKSOFSTUDENTS);
                                document.NewPage();

                                Paragraph eight = new Paragraph("LIST OF ALL STUDENTS WHO NEVER JOINED A GROUP\n\n", MainHeading);
                                eight.Alignment = Element.ALIGN_CENTER;
                                document.Add(eight);
                                document.Add(GROUPLESSsTUDENTS);
                                document.NewPage();

                                Paragraph twelwth = new Paragraph("TOP GROUP\n\n", MainHeading);
                                twelwth.Alignment = Element.ALIGN_CENTER;
                                document.Add(twelwth);
                                document.Add(TOPGROUP);
                                document.NewPage();


                                document.Close();

                                fileStream.Close();

                            }

                            MessageBox.Show("Data Export Successfully", "info");

                        }

                        catch (Exception ex)

                        {

                            MessageBox.Show("Error while exporting Data" + ex.Message);

                        }

                    }

                }

            }*/

    }
}
