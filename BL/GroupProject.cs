﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidProject.BL
{
    public class GroupProject
    {
        string projectId;
        string groupId;
        string assignmentDate;

        public GroupProject(string projectId, string groupId, string assignmentDate)
        {
            this.projectId = projectId;
            this.groupId = groupId;
            this.assignmentDate = assignmentDate;
        }

        public string ProjectId { get => projectId; set => projectId = value; }
        public string GroupId { get => groupId; set => groupId = value; }
        public string AssignmentDate { get => assignmentDate; set => assignmentDate = value; }
    }
}
