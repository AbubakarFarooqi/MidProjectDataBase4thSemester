﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidProject.BL
{
    public class Person
    {

        string id;
        string firstName;
        string lastName;
        string contact;
        string email;
        string dateOfBirth;
        string gender;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Contact { get => contact; set => contact = value; }
        public string Email { get => email; set => email = value; }
        public string DateOfBirth { get => dateOfBirth; set => dateOfBirth = value; }
        public string Gender { get => gender; set => gender = value; }
        public string Id { get => id; set => id = value; }

        public Person(string firstName, string lastName, string contact, string email,
                        string dateOfBirth, string gender)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Contact = contact;
            this.Email = email;
            this.DateOfBirth = parseDate(dateOfBirth);
            this.Gender = gender;
        }

        public Person(string id, string firstName, string lastName, string contact, string email, string dateOfBirth, string gender)
        {
            this.id = id;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Contact = contact;
            this.Email = email;
            this.Gender = gender;
            this.dateOfBirth = dateOfBirth;
        }

        private int parseMonthNumber(string month)
        {
            if (month == "January")
                return 1;
            if (month == "February")
                return 2;
            if (month == "March")
                return 3;
            if (month == "April")
                return 4;
            if (month == "May")
                return 5;
            if (month == "June")
                return 6;
            if (month == "July")
                return 7;
            if (month == "August")
                return 8;
            if (month == "September")
                return 9;
            if (month == "October")
                return 10;
            if (month == "November")
                return 11;
            if (month == "December")
                return 12;
            return -1;
        }

        public string parseDate(string date)
        {
            string[] temp = date.Split(',');
            string year = temp[2];
            string[] monthAndDay = temp[1].Split(' ');
            string month = monthAndDay[1];
            string day = monthAndDay[2];
            return year + "-" + parseMonthNumber(month) + "-" + day;

        }
    }
}
