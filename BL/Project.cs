﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidProject.BL
{
    public class Project
    {
        string id;
        string description;
        string title;


        /// <summary>
        /// constructs project from app layer
        /// </summary>
        /// <param name="description"></param>
        /// <param name="title"></param>
        public Project(string description, string title)
        {
            this.title = title;
            this.description = description;
        }

        /// <summary>
        /// construct project from database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="description"></param>
        /// <param name="title"></param>
        public Project(string id, string description, string title) : this(description, title)
        {
            this.id = id;
        }
        public string Id { get => id; set => id = value; }
        public string Description { get => description; set => description = value; }
        public string Title { get => title; set => title = value; }
    }
}
