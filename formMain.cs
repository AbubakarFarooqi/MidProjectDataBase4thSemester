﻿using MidProject.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace MidProject
{
    public partial class formMain : Form
    {

        bool isSubMenuIsShowing;
        private Form activeForm = null;
        public formMain()
        {
            InitializeComponent();
            isSubMenuIsShowing = false;
            hideAllSubmenu();
        }
        private void openChildForm(Form childForm)
        {
            try
            {
                if (activeForm != null) activeForm.Close();
                activeForm = childForm;
                childForm.TopLevel = false;
                childForm.FormBorderStyle = FormBorderStyle.None;
                childForm.Dock = DockStyle.Fill;
                pnlChildForm.Controls.Add(childForm);
                pnlChildForm.Tag = childForm;
                childForm.BringToFront();
                childForm.Show();
            }
            catch (Exception)
            {
            }

        }
        void hideAllSubmenu()
        {
            this.pnlSudentSubMenu.Hide();
            this.pnlAdvisorSubMenu.Hide();
            this.pnlProjectSubMenu.Hide();
            this.pnlGroupSubMenu.Hide();
            this.pnlEvaluationSubMenu.Hide();
            this.pnlGroupEvaluationSubMenu.Hide();
            this.pnlReportSubMenu.Hide();
        }
        private void guna2ImageButton1_Click(object sender, EventArgs e)
        {


        }
        void openSubMenu(Panel SubMenu)
        {
            hideAllSubmenu();
            if (isSubMenuIsShowing)
            {
                isSubMenuIsShowing = false;
            }
            else
            {
                SubMenu.Show();
                isSubMenuIsShowing = true;
            }
        }

        private void btnStudent_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formAddStudent());

        }

        private void btnDeleteStudent_Click(object sender, EventArgs e)
        {

        }

        private void btnStudent_Click_1(object sender, EventArgs e)
        {
            openSubMenu(this.pnlSudentSubMenu);
        }

        private void btnManageStudents_Click(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formManageStudents());
        }

        private void btnAdvisors_Click(object sender, EventArgs e)
        {
            openSubMenu(this.pnlAdvisorSubMenu);
        }

        private void btnAddAdvisor_Click(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formAddAdvisor());
        }

        private void btnManageAdvisor_Click(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formManageAdvisor());
        }

        private void btnAddProject_Click(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formAddProject());
        }

        private void btnProject_Click(object sender, EventArgs e)
        {
            openSubMenu(this.pnlProjectSubMenu);
        }

        private void btnManagaeProject_Click(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formManageProject());
        }

        private void guna2Button1_Click_1(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formManageGroup(pnlChildForm));
        }

        private void btnGroups_Click(object sender, EventArgs e)
        {
            openSubMenu(this.pnlGroupSubMenu);
        }

        private void btnCreateGroup_Click(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formCreateGroup());
        }

        private void btnAssignProject_Click(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formAssignProjectToGroup());
        }

        private void formMain_Load(object sender, EventArgs e)
        {

        }

        private void btnCreateEvaluation_Click(object sender, EventArgs e)
        {

            hideAllSubmenu();
            openChildForm(new formCreateEvaluation());
        }

        private void btnEvaluation_Click(object sender, EventArgs e)
        {
            openSubMenu(this.pnlEvaluationSubMenu);
        }

        private void btnManageEvaluations_Click(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formManageEvaluations());
        }

        private void guna2Button1_Click_2(object sender, EventArgs e)
        {
            openSubMenu(this.pnlGroupEvaluationSubMenu);
        }

        private void btnMarkEvaluation_Click(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formMarkEvaluation(pnlChildForm));
        }

        private void btnEditGroupEvaluation_Click(object sender, EventArgs e)
        {
            hideAllSubmenu();
            openChildForm(new formEditGroupEvaluation(pnlChildForm));
        }

        private SaveFileDialog resultOfSaveDialog()
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "PDF (*.pdf)|*.pdf";
            save.FileName = "Report.pdf";

            if (save.ShowDialog() != DialogResult.OK)
                return null;
            return save;
        }
        private void guna2Button1_Click_3(object sender, EventArgs e)
        {

            openSubMenu(this.pnlReportSubMenu);

        }

        private void btnAllStudents_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = resultOfSaveDialog();
            if (save != null)
            {
                Report rep = new Report();
                FileStream fs = rep.getStream(save);
                if (fs == null)
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                if (!rep.generateListOfAllStudents(fs))
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                else
                {
                    msgBox.Caption = Messages.CAPTION_SUCCESS;
                    msgBox.Text = Messages.REPORT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                    msgBox.Show();
                    return;
                }


            }
            else
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.REPORT_NOT_GENERATED;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
            }
        }

        private void btnAllAdvisors_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = resultOfSaveDialog();
            if (save != null)
            {
                Report rep = new Report();
                FileStream fs = rep.getStream(save);
                if (fs == null)
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                if (!rep.generateListOfAllAdvisors(fs))
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                else
                {
                    msgBox.Caption = Messages.CAPTION_SUCCESS;
                    msgBox.Text = Messages.REPORT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                    msgBox.Show();
                    return;
                }


            }
            else
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.REPORT_NOT_GENERATED;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
            }
        }

        private void btnProjectsNotAssignedToAnyGroup_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = resultOfSaveDialog();
            if (save != null)
            {
                Report rep = new Report();
                FileStream fs = rep.getStream(save);
                if (fs == null)
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                if (!rep.generateNonGroupProjects(fs))
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                else
                {
                    msgBox.Caption = Messages.CAPTION_SUCCESS;
                    msgBox.Text = Messages.REPORT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                    msgBox.Show();
                    return;
                }


            }
            else
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.REPORT_NOT_GENERATED;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
            }
        }

        private void btnNonGroupStudents_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = resultOfSaveDialog();
            if (save != null)
            {
                Report rep = new Report();
                FileStream fs = rep.getStream(save);
                if (fs == null)
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                if (!rep.generateNonGroupStudents(fs))
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                else
                {
                    msgBox.Caption = Messages.CAPTION_SUCCESS;
                    msgBox.Text = Messages.REPORT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                    msgBox.Show();
                    return;
                }


            }
            else
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.REPORT_NOT_GENERATED;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
            }
        }

        private void btnNonProjectAdvisor_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = resultOfSaveDialog();
            if (save != null)
            {
                Report rep = new Report();
                FileStream fs = rep.getStream(save);
                if (fs == null)
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                if (!rep.generateNonProjectAdvisors(fs))
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                else
                {
                    msgBox.Caption = Messages.CAPTION_SUCCESS;
                    msgBox.Text = Messages.REPORT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                    msgBox.Show();
                    return;
                }


            }
            else
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.REPORT_NOT_GENERATED;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
            }
        }

        private void btnEvaluationStatus_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = resultOfSaveDialog();
            if (save != null)
            {
                Report rep = new Report();
                FileStream fs = rep.getStream(save);
                if (fs == null)
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                if (!rep.generateEvalutionsStatus(fs))
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                else
                {
                    msgBox.Caption = Messages.CAPTION_SUCCESS;
                    msgBox.Text = Messages.REPORT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                    msgBox.Show();
                    return;
                }


            }
            else
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.REPORT_NOT_GENERATED;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
            }
        }

        private void btnGroupEvaluationStatus_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = resultOfSaveDialog();
            if (save != null)
            {
                Report rep = new Report();
                FileStream fs = rep.getStream(save);
                if (fs == null)
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                if (!rep.generateGroupWiseEvalutionsResult(fs))
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Text = Messages.REPORT_NOT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Show();
                    return;
                }
                else
                {
                    msgBox.Caption = Messages.CAPTION_SUCCESS;
                    msgBox.Text = Messages.REPORT_GENERATED;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                    msgBox.Show();
                    return;
                }


            }
            else
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.REPORT_NOT_GENERATED;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
            }
        }
    }
}
