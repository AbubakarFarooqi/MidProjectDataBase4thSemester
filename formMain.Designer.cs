﻿
namespace MidProject
{
    partial class formMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formMain));
            this.pnlSideBar = new System.Windows.Forms.Panel();
            this.pnlReportSubMenu = new System.Windows.Forms.Panel();
            this.btnEvaluationStatus = new Guna.UI2.WinForms.Guna2Button();
            this.btnNonProjectAdvisor = new Guna.UI2.WinForms.Guna2Button();
            this.btnNonGroupStudents = new Guna.UI2.WinForms.Guna2Button();
            this.btnProjectsNotAssignedToAnyGroup = new Guna.UI2.WinForms.Guna2Button();
            this.btnAllAdvisors = new Guna.UI2.WinForms.Guna2Button();
            this.btnAllStudents = new Guna.UI2.WinForms.Guna2Button();
            this.btnReports = new Guna.UI2.WinForms.Guna2Button();
            this.pnlGroupEvaluationSubMenu = new System.Windows.Forms.Panel();
            this.btnEditGroupEvaluation = new Guna.UI2.WinForms.Guna2Button();
            this.btnMarkEvaluation = new Guna.UI2.WinForms.Guna2Button();
            this.btnGroupEvaluation = new Guna.UI2.WinForms.Guna2Button();
            this.pnlEvaluationSubMenu = new System.Windows.Forms.Panel();
            this.btnManageEvaluations = new Guna.UI2.WinForms.Guna2Button();
            this.btnCreateEvaluation = new Guna.UI2.WinForms.Guna2Button();
            this.btnEvaluation = new Guna.UI2.WinForms.Guna2Button();
            this.pnlGroupSubMenu = new System.Windows.Forms.Panel();
            this.btnAssignProject = new Guna.UI2.WinForms.Guna2Button();
            this.btnDeleteGroup = new Guna.UI2.WinForms.Guna2Button();
            this.btnCreateGroup = new Guna.UI2.WinForms.Guna2Button();
            this.btnGroups = new Guna.UI2.WinForms.Guna2Button();
            this.pnlProjectSubMenu = new System.Windows.Forms.Panel();
            this.btnManagaeProject = new Guna.UI2.WinForms.Guna2Button();
            this.btnAddProject = new Guna.UI2.WinForms.Guna2Button();
            this.btnProject = new Guna.UI2.WinForms.Guna2Button();
            this.pnlAdvisorSubMenu = new System.Windows.Forms.Panel();
            this.btnManageAdvisor = new Guna.UI2.WinForms.Guna2Button();
            this.btnAddAdvisor = new Guna.UI2.WinForms.Guna2Button();
            this.btnAdvisors = new Guna.UI2.WinForms.Guna2Button();
            this.pnlSudentSubMenu = new System.Windows.Forms.Panel();
            this.btnManageStudents = new Guna.UI2.WinForms.Guna2Button();
            this.btnAddStudent = new Guna.UI2.WinForms.Guna2Button();
            this.btnStudent = new Guna.UI2.WinForms.Guna2Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlMenuHeader = new System.Windows.Forms.Panel();
            this.guna2PictureBox2 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.pnlChildForm = new System.Windows.Forms.Panel();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.msgBox = new Guna.UI2.WinForms.Guna2MessageDialog();
            this.btnGroupEvaluationStatus = new Guna.UI2.WinForms.Guna2Button();
            this.pnlSideBar.SuspendLayout();
            this.pnlReportSubMenu.SuspendLayout();
            this.pnlGroupEvaluationSubMenu.SuspendLayout();
            this.pnlEvaluationSubMenu.SuspendLayout();
            this.pnlGroupSubMenu.SuspendLayout();
            this.pnlProjectSubMenu.SuspendLayout();
            this.pnlAdvisorSubMenu.SuspendLayout();
            this.pnlSudentSubMenu.SuspendLayout();
            this.pnlMenuHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).BeginInit();
            this.pnlChildForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSideBar
            // 
            this.pnlSideBar.AutoScroll = true;
            this.pnlSideBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.pnlSideBar.Controls.Add(this.pnlReportSubMenu);
            this.pnlSideBar.Controls.Add(this.btnReports);
            this.pnlSideBar.Controls.Add(this.pnlGroupEvaluationSubMenu);
            this.pnlSideBar.Controls.Add(this.btnGroupEvaluation);
            this.pnlSideBar.Controls.Add(this.pnlEvaluationSubMenu);
            this.pnlSideBar.Controls.Add(this.btnEvaluation);
            this.pnlSideBar.Controls.Add(this.pnlGroupSubMenu);
            this.pnlSideBar.Controls.Add(this.btnGroups);
            this.pnlSideBar.Controls.Add(this.pnlProjectSubMenu);
            this.pnlSideBar.Controls.Add(this.btnProject);
            this.pnlSideBar.Controls.Add(this.pnlAdvisorSubMenu);
            this.pnlSideBar.Controls.Add(this.btnAdvisors);
            this.pnlSideBar.Controls.Add(this.pnlSudentSubMenu);
            this.pnlSideBar.Controls.Add(this.btnStudent);
            this.pnlSideBar.Controls.Add(this.btnExit);
            this.pnlSideBar.Controls.Add(this.pnlMenuHeader);
            this.pnlSideBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlSideBar.Location = new System.Drawing.Point(0, 0);
            this.pnlSideBar.MaximumSize = new System.Drawing.Size(200, 0);
            this.pnlSideBar.Name = "pnlSideBar";
            this.pnlSideBar.Size = new System.Drawing.Size(200, 691);
            this.pnlSideBar.TabIndex = 0;
            // 
            // pnlReportSubMenu
            // 
            this.pnlReportSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.pnlReportSubMenu.Controls.Add(this.btnGroupEvaluationStatus);
            this.pnlReportSubMenu.Controls.Add(this.btnEvaluationStatus);
            this.pnlReportSubMenu.Controls.Add(this.btnNonProjectAdvisor);
            this.pnlReportSubMenu.Controls.Add(this.btnNonGroupStudents);
            this.pnlReportSubMenu.Controls.Add(this.btnProjectsNotAssignedToAnyGroup);
            this.pnlReportSubMenu.Controls.Add(this.btnAllAdvisors);
            this.pnlReportSubMenu.Controls.Add(this.btnAllStudents);
            this.pnlReportSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlReportSubMenu.Location = new System.Drawing.Point(0, 874);
            this.pnlReportSubMenu.Name = "pnlReportSubMenu";
            this.pnlReportSubMenu.Size = new System.Drawing.Size(183, 215);
            this.pnlReportSubMenu.TabIndex = 24;
            // 
            // btnEvaluationStatus
            // 
            this.btnEvaluationStatus.Animated = true;
            this.btnEvaluationStatus.BackColor = System.Drawing.Color.Transparent;
            this.btnEvaluationStatus.DefaultAutoSize = true;
            this.btnEvaluationStatus.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnEvaluationStatus.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnEvaluationStatus.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnEvaluationStatus.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnEvaluationStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEvaluationStatus.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnEvaluationStatus.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnEvaluationStatus.ForeColor = System.Drawing.Color.White;
            this.btnEvaluationStatus.IndicateFocus = true;
            this.btnEvaluationStatus.Location = new System.Drawing.Point(0, 135);
            this.btnEvaluationStatus.Name = "btnEvaluationStatus";
            this.btnEvaluationStatus.Size = new System.Drawing.Size(183, 27);
            this.btnEvaluationStatus.TabIndex = 19;
            this.btnEvaluationStatus.Text = "    Evaluation Status";
            this.btnEvaluationStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnEvaluationStatus.UseTransparentBackground = true;
            this.btnEvaluationStatus.Click += new System.EventHandler(this.btnEvaluationStatus_Click);
            // 
            // btnNonProjectAdvisor
            // 
            this.btnNonProjectAdvisor.Animated = true;
            this.btnNonProjectAdvisor.BackColor = System.Drawing.Color.Transparent;
            this.btnNonProjectAdvisor.DefaultAutoSize = true;
            this.btnNonProjectAdvisor.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnNonProjectAdvisor.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnNonProjectAdvisor.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnNonProjectAdvisor.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnNonProjectAdvisor.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnNonProjectAdvisor.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnNonProjectAdvisor.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnNonProjectAdvisor.ForeColor = System.Drawing.Color.White;
            this.btnNonProjectAdvisor.IndicateFocus = true;
            this.btnNonProjectAdvisor.Location = new System.Drawing.Point(0, 108);
            this.btnNonProjectAdvisor.Name = "btnNonProjectAdvisor";
            this.btnNonProjectAdvisor.Size = new System.Drawing.Size(183, 27);
            this.btnNonProjectAdvisor.TabIndex = 18;
            this.btnNonProjectAdvisor.Text = "    Non Project Advisor";
            this.btnNonProjectAdvisor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnNonProjectAdvisor.UseTransparentBackground = true;
            this.btnNonProjectAdvisor.Click += new System.EventHandler(this.btnNonProjectAdvisor_Click);
            // 
            // btnNonGroupStudents
            // 
            this.btnNonGroupStudents.Animated = true;
            this.btnNonGroupStudents.BackColor = System.Drawing.Color.Transparent;
            this.btnNonGroupStudents.DefaultAutoSize = true;
            this.btnNonGroupStudents.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnNonGroupStudents.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnNonGroupStudents.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnNonGroupStudents.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnNonGroupStudents.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnNonGroupStudents.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnNonGroupStudents.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnNonGroupStudents.ForeColor = System.Drawing.Color.White;
            this.btnNonGroupStudents.IndicateFocus = true;
            this.btnNonGroupStudents.Location = new System.Drawing.Point(0, 81);
            this.btnNonGroupStudents.Name = "btnNonGroupStudents";
            this.btnNonGroupStudents.Size = new System.Drawing.Size(183, 27);
            this.btnNonGroupStudents.TabIndex = 17;
            this.btnNonGroupStudents.Text = "    Non Group Students";
            this.btnNonGroupStudents.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnNonGroupStudents.UseTransparentBackground = true;
            this.btnNonGroupStudents.Click += new System.EventHandler(this.btnNonGroupStudents_Click);
            // 
            // btnProjectsNotAssignedToAnyGroup
            // 
            this.btnProjectsNotAssignedToAnyGroup.Animated = true;
            this.btnProjectsNotAssignedToAnyGroup.BackColor = System.Drawing.Color.Transparent;
            this.btnProjectsNotAssignedToAnyGroup.DefaultAutoSize = true;
            this.btnProjectsNotAssignedToAnyGroup.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnProjectsNotAssignedToAnyGroup.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnProjectsNotAssignedToAnyGroup.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnProjectsNotAssignedToAnyGroup.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnProjectsNotAssignedToAnyGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProjectsNotAssignedToAnyGroup.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnProjectsNotAssignedToAnyGroup.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnProjectsNotAssignedToAnyGroup.ForeColor = System.Drawing.Color.White;
            this.btnProjectsNotAssignedToAnyGroup.IndicateFocus = true;
            this.btnProjectsNotAssignedToAnyGroup.Location = new System.Drawing.Point(0, 54);
            this.btnProjectsNotAssignedToAnyGroup.Name = "btnProjectsNotAssignedToAnyGroup";
            this.btnProjectsNotAssignedToAnyGroup.Size = new System.Drawing.Size(183, 27);
            this.btnProjectsNotAssignedToAnyGroup.TabIndex = 16;
            this.btnProjectsNotAssignedToAnyGroup.Text = "    Non Group Projects";
            this.btnProjectsNotAssignedToAnyGroup.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnProjectsNotAssignedToAnyGroup.UseTransparentBackground = true;
            this.btnProjectsNotAssignedToAnyGroup.Click += new System.EventHandler(this.btnProjectsNotAssignedToAnyGroup_Click);
            // 
            // btnAllAdvisors
            // 
            this.btnAllAdvisors.Animated = true;
            this.btnAllAdvisors.BackColor = System.Drawing.Color.Transparent;
            this.btnAllAdvisors.DefaultAutoSize = true;
            this.btnAllAdvisors.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnAllAdvisors.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnAllAdvisors.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnAllAdvisors.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnAllAdvisors.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAllAdvisors.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnAllAdvisors.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnAllAdvisors.ForeColor = System.Drawing.Color.White;
            this.btnAllAdvisors.IndicateFocus = true;
            this.btnAllAdvisors.Location = new System.Drawing.Point(0, 27);
            this.btnAllAdvisors.Name = "btnAllAdvisors";
            this.btnAllAdvisors.Size = new System.Drawing.Size(183, 27);
            this.btnAllAdvisors.TabIndex = 15;
            this.btnAllAdvisors.Text = "    All Advisors";
            this.btnAllAdvisors.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnAllAdvisors.UseTransparentBackground = true;
            this.btnAllAdvisors.Click += new System.EventHandler(this.btnAllAdvisors_Click);
            // 
            // btnAllStudents
            // 
            this.btnAllStudents.Animated = true;
            this.btnAllStudents.BackColor = System.Drawing.Color.Transparent;
            this.btnAllStudents.DefaultAutoSize = true;
            this.btnAllStudents.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnAllStudents.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnAllStudents.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnAllStudents.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnAllStudents.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAllStudents.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnAllStudents.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnAllStudents.ForeColor = System.Drawing.Color.White;
            this.btnAllStudents.IndicateFocus = true;
            this.btnAllStudents.Location = new System.Drawing.Point(0, 0);
            this.btnAllStudents.Name = "btnAllStudents";
            this.btnAllStudents.Size = new System.Drawing.Size(183, 27);
            this.btnAllStudents.TabIndex = 12;
            this.btnAllStudents.Text = "    All Students";
            this.btnAllStudents.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnAllStudents.UseTransparentBackground = true;
            this.btnAllStudents.Click += new System.EventHandler(this.btnAllStudents_Click);
            // 
            // btnReports
            // 
            this.btnReports.Animated = true;
            this.btnReports.BackColor = System.Drawing.Color.Transparent;
            this.btnReports.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnReports.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnReports.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnReports.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnReports.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnReports.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.btnReports.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnReports.ForeColor = System.Drawing.Color.White;
            this.btnReports.Image = ((System.Drawing.Image)(resources.GetObject("btnReports.Image")));
            this.btnReports.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnReports.ImageSize = new System.Drawing.Size(35, 35);
            this.btnReports.IndicateFocus = true;
            this.btnReports.Location = new System.Drawing.Point(0, 827);
            this.btnReports.Name = "btnReports";
            this.btnReports.Size = new System.Drawing.Size(183, 47);
            this.btnReports.TabIndex = 23;
            this.btnReports.Text = "Reports";
            this.btnReports.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnReports.UseTransparentBackground = true;
            this.btnReports.Click += new System.EventHandler(this.guna2Button1_Click_3);
            // 
            // pnlGroupEvaluationSubMenu
            // 
            this.pnlGroupEvaluationSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.pnlGroupEvaluationSubMenu.Controls.Add(this.btnEditGroupEvaluation);
            this.pnlGroupEvaluationSubMenu.Controls.Add(this.btnMarkEvaluation);
            this.pnlGroupEvaluationSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlGroupEvaluationSubMenu.Location = new System.Drawing.Point(0, 767);
            this.pnlGroupEvaluationSubMenu.Name = "pnlGroupEvaluationSubMenu";
            this.pnlGroupEvaluationSubMenu.Size = new System.Drawing.Size(183, 60);
            this.pnlGroupEvaluationSubMenu.TabIndex = 22;
            // 
            // btnEditGroupEvaluation
            // 
            this.btnEditGroupEvaluation.Animated = true;
            this.btnEditGroupEvaluation.BackColor = System.Drawing.Color.Transparent;
            this.btnEditGroupEvaluation.DefaultAutoSize = true;
            this.btnEditGroupEvaluation.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnEditGroupEvaluation.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnEditGroupEvaluation.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnEditGroupEvaluation.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnEditGroupEvaluation.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEditGroupEvaluation.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnEditGroupEvaluation.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnEditGroupEvaluation.ForeColor = System.Drawing.Color.White;
            this.btnEditGroupEvaluation.IndicateFocus = true;
            this.btnEditGroupEvaluation.Location = new System.Drawing.Point(0, 27);
            this.btnEditGroupEvaluation.Name = "btnEditGroupEvaluation";
            this.btnEditGroupEvaluation.Size = new System.Drawing.Size(183, 27);
            this.btnEditGroupEvaluation.TabIndex = 15;
            this.btnEditGroupEvaluation.Text = "    Edit Group Evaluation";
            this.btnEditGroupEvaluation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnEditGroupEvaluation.UseTransparentBackground = true;
            this.btnEditGroupEvaluation.Click += new System.EventHandler(this.btnEditGroupEvaluation_Click);
            // 
            // btnMarkEvaluation
            // 
            this.btnMarkEvaluation.Animated = true;
            this.btnMarkEvaluation.BackColor = System.Drawing.Color.Transparent;
            this.btnMarkEvaluation.DefaultAutoSize = true;
            this.btnMarkEvaluation.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnMarkEvaluation.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnMarkEvaluation.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnMarkEvaluation.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnMarkEvaluation.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMarkEvaluation.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnMarkEvaluation.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnMarkEvaluation.ForeColor = System.Drawing.Color.White;
            this.btnMarkEvaluation.IndicateFocus = true;
            this.btnMarkEvaluation.Location = new System.Drawing.Point(0, 0);
            this.btnMarkEvaluation.Name = "btnMarkEvaluation";
            this.btnMarkEvaluation.Size = new System.Drawing.Size(183, 27);
            this.btnMarkEvaluation.TabIndex = 12;
            this.btnMarkEvaluation.Text = "    Mark Evaluation";
            this.btnMarkEvaluation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnMarkEvaluation.UseTransparentBackground = true;
            this.btnMarkEvaluation.Click += new System.EventHandler(this.btnMarkEvaluation_Click);
            // 
            // btnGroupEvaluation
            // 
            this.btnGroupEvaluation.Animated = true;
            this.btnGroupEvaluation.BackColor = System.Drawing.Color.Transparent;
            this.btnGroupEvaluation.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnGroupEvaluation.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnGroupEvaluation.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnGroupEvaluation.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnGroupEvaluation.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGroupEvaluation.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.btnGroupEvaluation.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnGroupEvaluation.ForeColor = System.Drawing.Color.White;
            this.btnGroupEvaluation.Image = ((System.Drawing.Image)(resources.GetObject("btnGroupEvaluation.Image")));
            this.btnGroupEvaluation.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnGroupEvaluation.ImageSize = new System.Drawing.Size(35, 35);
            this.btnGroupEvaluation.IndicateFocus = true;
            this.btnGroupEvaluation.Location = new System.Drawing.Point(0, 720);
            this.btnGroupEvaluation.Name = "btnGroupEvaluation";
            this.btnGroupEvaluation.Size = new System.Drawing.Size(183, 47);
            this.btnGroupEvaluation.TabIndex = 21;
            this.btnGroupEvaluation.Text = "Group Evaluation";
            this.btnGroupEvaluation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnGroupEvaluation.UseTransparentBackground = true;
            this.btnGroupEvaluation.Click += new System.EventHandler(this.guna2Button1_Click_2);
            // 
            // pnlEvaluationSubMenu
            // 
            this.pnlEvaluationSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.pnlEvaluationSubMenu.Controls.Add(this.btnManageEvaluations);
            this.pnlEvaluationSubMenu.Controls.Add(this.btnCreateEvaluation);
            this.pnlEvaluationSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlEvaluationSubMenu.Location = new System.Drawing.Point(0, 660);
            this.pnlEvaluationSubMenu.Name = "pnlEvaluationSubMenu";
            this.pnlEvaluationSubMenu.Size = new System.Drawing.Size(183, 60);
            this.pnlEvaluationSubMenu.TabIndex = 20;
            // 
            // btnManageEvaluations
            // 
            this.btnManageEvaluations.Animated = true;
            this.btnManageEvaluations.BackColor = System.Drawing.Color.Transparent;
            this.btnManageEvaluations.DefaultAutoSize = true;
            this.btnManageEvaluations.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnManageEvaluations.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnManageEvaluations.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnManageEvaluations.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnManageEvaluations.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManageEvaluations.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnManageEvaluations.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnManageEvaluations.ForeColor = System.Drawing.Color.White;
            this.btnManageEvaluations.IndicateFocus = true;
            this.btnManageEvaluations.Location = new System.Drawing.Point(0, 27);
            this.btnManageEvaluations.Name = "btnManageEvaluations";
            this.btnManageEvaluations.Size = new System.Drawing.Size(183, 27);
            this.btnManageEvaluations.TabIndex = 15;
            this.btnManageEvaluations.Text = "    Manage Evaluations";
            this.btnManageEvaluations.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnManageEvaluations.UseTransparentBackground = true;
            this.btnManageEvaluations.Click += new System.EventHandler(this.btnManageEvaluations_Click);
            // 
            // btnCreateEvaluation
            // 
            this.btnCreateEvaluation.Animated = true;
            this.btnCreateEvaluation.BackColor = System.Drawing.Color.Transparent;
            this.btnCreateEvaluation.DefaultAutoSize = true;
            this.btnCreateEvaluation.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnCreateEvaluation.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnCreateEvaluation.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnCreateEvaluation.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnCreateEvaluation.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCreateEvaluation.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnCreateEvaluation.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnCreateEvaluation.ForeColor = System.Drawing.Color.White;
            this.btnCreateEvaluation.IndicateFocus = true;
            this.btnCreateEvaluation.Location = new System.Drawing.Point(0, 0);
            this.btnCreateEvaluation.Name = "btnCreateEvaluation";
            this.btnCreateEvaluation.Size = new System.Drawing.Size(183, 27);
            this.btnCreateEvaluation.TabIndex = 12;
            this.btnCreateEvaluation.Text = "    Create Evaluation";
            this.btnCreateEvaluation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnCreateEvaluation.UseTransparentBackground = true;
            this.btnCreateEvaluation.Click += new System.EventHandler(this.btnCreateEvaluation_Click);
            // 
            // btnEvaluation
            // 
            this.btnEvaluation.Animated = true;
            this.btnEvaluation.BackColor = System.Drawing.Color.Transparent;
            this.btnEvaluation.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnEvaluation.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnEvaluation.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnEvaluation.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnEvaluation.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEvaluation.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.btnEvaluation.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnEvaluation.ForeColor = System.Drawing.Color.White;
            this.btnEvaluation.Image = ((System.Drawing.Image)(resources.GetObject("btnEvaluation.Image")));
            this.btnEvaluation.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnEvaluation.ImageSize = new System.Drawing.Size(35, 35);
            this.btnEvaluation.IndicateFocus = true;
            this.btnEvaluation.Location = new System.Drawing.Point(0, 613);
            this.btnEvaluation.Name = "btnEvaluation";
            this.btnEvaluation.Size = new System.Drawing.Size(183, 47);
            this.btnEvaluation.TabIndex = 19;
            this.btnEvaluation.Text = "Evaluations";
            this.btnEvaluation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnEvaluation.UseTransparentBackground = true;
            this.btnEvaluation.Click += new System.EventHandler(this.btnEvaluation_Click);
            // 
            // pnlGroupSubMenu
            // 
            this.pnlGroupSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.pnlGroupSubMenu.Controls.Add(this.btnAssignProject);
            this.pnlGroupSubMenu.Controls.Add(this.btnDeleteGroup);
            this.pnlGroupSubMenu.Controls.Add(this.btnCreateGroup);
            this.pnlGroupSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlGroupSubMenu.Location = new System.Drawing.Point(0, 532);
            this.pnlGroupSubMenu.Name = "pnlGroupSubMenu";
            this.pnlGroupSubMenu.Size = new System.Drawing.Size(183, 81);
            this.pnlGroupSubMenu.TabIndex = 18;
            // 
            // btnAssignProject
            // 
            this.btnAssignProject.Animated = true;
            this.btnAssignProject.BackColor = System.Drawing.Color.Transparent;
            this.btnAssignProject.DefaultAutoSize = true;
            this.btnAssignProject.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnAssignProject.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnAssignProject.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnAssignProject.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnAssignProject.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAssignProject.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnAssignProject.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnAssignProject.ForeColor = System.Drawing.Color.White;
            this.btnAssignProject.IndicateFocus = true;
            this.btnAssignProject.Location = new System.Drawing.Point(0, 54);
            this.btnAssignProject.Name = "btnAssignProject";
            this.btnAssignProject.Size = new System.Drawing.Size(183, 27);
            this.btnAssignProject.TabIndex = 16;
            this.btnAssignProject.Text = "    Assign Project";
            this.btnAssignProject.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnAssignProject.UseTransparentBackground = true;
            this.btnAssignProject.Click += new System.EventHandler(this.btnAssignProject_Click);
            // 
            // btnDeleteGroup
            // 
            this.btnDeleteGroup.Animated = true;
            this.btnDeleteGroup.BackColor = System.Drawing.Color.Transparent;
            this.btnDeleteGroup.DefaultAutoSize = true;
            this.btnDeleteGroup.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnDeleteGroup.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnDeleteGroup.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnDeleteGroup.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnDeleteGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDeleteGroup.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnDeleteGroup.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnDeleteGroup.ForeColor = System.Drawing.Color.White;
            this.btnDeleteGroup.IndicateFocus = true;
            this.btnDeleteGroup.Location = new System.Drawing.Point(0, 27);
            this.btnDeleteGroup.Name = "btnDeleteGroup";
            this.btnDeleteGroup.Size = new System.Drawing.Size(183, 27);
            this.btnDeleteGroup.TabIndex = 15;
            this.btnDeleteGroup.Text = "    Manage Group";
            this.btnDeleteGroup.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnDeleteGroup.UseTransparentBackground = true;
            this.btnDeleteGroup.Click += new System.EventHandler(this.guna2Button1_Click_1);
            // 
            // btnCreateGroup
            // 
            this.btnCreateGroup.Animated = true;
            this.btnCreateGroup.BackColor = System.Drawing.Color.Transparent;
            this.btnCreateGroup.DefaultAutoSize = true;
            this.btnCreateGroup.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnCreateGroup.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnCreateGroup.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnCreateGroup.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnCreateGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCreateGroup.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnCreateGroup.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnCreateGroup.ForeColor = System.Drawing.Color.White;
            this.btnCreateGroup.IndicateFocus = true;
            this.btnCreateGroup.Location = new System.Drawing.Point(0, 0);
            this.btnCreateGroup.Name = "btnCreateGroup";
            this.btnCreateGroup.Size = new System.Drawing.Size(183, 27);
            this.btnCreateGroup.TabIndex = 12;
            this.btnCreateGroup.Text = "    Create Group";
            this.btnCreateGroup.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnCreateGroup.UseTransparentBackground = true;
            this.btnCreateGroup.Click += new System.EventHandler(this.btnCreateGroup_Click);
            // 
            // btnGroups
            // 
            this.btnGroups.Animated = true;
            this.btnGroups.BackColor = System.Drawing.Color.Transparent;
            this.btnGroups.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnGroups.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnGroups.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnGroups.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnGroups.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGroups.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.btnGroups.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnGroups.ForeColor = System.Drawing.Color.White;
            this.btnGroups.Image = ((System.Drawing.Image)(resources.GetObject("btnGroups.Image")));
            this.btnGroups.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnGroups.ImageSize = new System.Drawing.Size(35, 35);
            this.btnGroups.IndicateFocus = true;
            this.btnGroups.Location = new System.Drawing.Point(0, 485);
            this.btnGroups.Name = "btnGroups";
            this.btnGroups.Size = new System.Drawing.Size(183, 47);
            this.btnGroups.TabIndex = 17;
            this.btnGroups.Text = "Groups";
            this.btnGroups.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnGroups.UseTransparentBackground = true;
            this.btnGroups.Click += new System.EventHandler(this.btnGroups_Click);
            // 
            // pnlProjectSubMenu
            // 
            this.pnlProjectSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.pnlProjectSubMenu.Controls.Add(this.btnManagaeProject);
            this.pnlProjectSubMenu.Controls.Add(this.btnAddProject);
            this.pnlProjectSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlProjectSubMenu.Location = new System.Drawing.Point(0, 424);
            this.pnlProjectSubMenu.Name = "pnlProjectSubMenu";
            this.pnlProjectSubMenu.Size = new System.Drawing.Size(183, 61);
            this.pnlProjectSubMenu.TabIndex = 16;
            // 
            // btnManagaeProject
            // 
            this.btnManagaeProject.Animated = true;
            this.btnManagaeProject.BackColor = System.Drawing.Color.Transparent;
            this.btnManagaeProject.DefaultAutoSize = true;
            this.btnManagaeProject.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnManagaeProject.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnManagaeProject.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnManagaeProject.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnManagaeProject.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManagaeProject.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnManagaeProject.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnManagaeProject.ForeColor = System.Drawing.Color.White;
            this.btnManagaeProject.IndicateFocus = true;
            this.btnManagaeProject.Location = new System.Drawing.Point(0, 27);
            this.btnManagaeProject.Name = "btnManagaeProject";
            this.btnManagaeProject.Size = new System.Drawing.Size(183, 27);
            this.btnManagaeProject.TabIndex = 15;
            this.btnManagaeProject.Text = "    Manage Project";
            this.btnManagaeProject.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnManagaeProject.UseTransparentBackground = true;
            this.btnManagaeProject.Click += new System.EventHandler(this.btnManagaeProject_Click);
            // 
            // btnAddProject
            // 
            this.btnAddProject.Animated = true;
            this.btnAddProject.BackColor = System.Drawing.Color.Transparent;
            this.btnAddProject.DefaultAutoSize = true;
            this.btnAddProject.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnAddProject.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnAddProject.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnAddProject.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnAddProject.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddProject.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnAddProject.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnAddProject.ForeColor = System.Drawing.Color.White;
            this.btnAddProject.IndicateFocus = true;
            this.btnAddProject.Location = new System.Drawing.Point(0, 0);
            this.btnAddProject.Name = "btnAddProject";
            this.btnAddProject.Size = new System.Drawing.Size(183, 27);
            this.btnAddProject.TabIndex = 12;
            this.btnAddProject.Text = "    Add Project";
            this.btnAddProject.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnAddProject.UseTransparentBackground = true;
            this.btnAddProject.Click += new System.EventHandler(this.btnAddProject_Click);
            // 
            // btnProject
            // 
            this.btnProject.Animated = true;
            this.btnProject.BackColor = System.Drawing.Color.Transparent;
            this.btnProject.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnProject.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnProject.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnProject.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnProject.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProject.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.btnProject.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnProject.ForeColor = System.Drawing.Color.White;
            this.btnProject.Image = ((System.Drawing.Image)(resources.GetObject("btnProject.Image")));
            this.btnProject.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnProject.ImageSize = new System.Drawing.Size(35, 35);
            this.btnProject.IndicateFocus = true;
            this.btnProject.Location = new System.Drawing.Point(0, 377);
            this.btnProject.Name = "btnProject";
            this.btnProject.Size = new System.Drawing.Size(183, 47);
            this.btnProject.TabIndex = 15;
            this.btnProject.Text = "Projects";
            this.btnProject.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnProject.UseTransparentBackground = true;
            this.btnProject.Click += new System.EventHandler(this.btnProject_Click);
            // 
            // pnlAdvisorSubMenu
            // 
            this.pnlAdvisorSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.pnlAdvisorSubMenu.Controls.Add(this.btnManageAdvisor);
            this.pnlAdvisorSubMenu.Controls.Add(this.btnAddAdvisor);
            this.pnlAdvisorSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAdvisorSubMenu.Location = new System.Drawing.Point(0, 316);
            this.pnlAdvisorSubMenu.Name = "pnlAdvisorSubMenu";
            this.pnlAdvisorSubMenu.Size = new System.Drawing.Size(183, 61);
            this.pnlAdvisorSubMenu.TabIndex = 14;
            // 
            // btnManageAdvisor
            // 
            this.btnManageAdvisor.Animated = true;
            this.btnManageAdvisor.BackColor = System.Drawing.Color.Transparent;
            this.btnManageAdvisor.DefaultAutoSize = true;
            this.btnManageAdvisor.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnManageAdvisor.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnManageAdvisor.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnManageAdvisor.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnManageAdvisor.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManageAdvisor.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnManageAdvisor.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnManageAdvisor.ForeColor = System.Drawing.Color.White;
            this.btnManageAdvisor.IndicateFocus = true;
            this.btnManageAdvisor.Location = new System.Drawing.Point(0, 27);
            this.btnManageAdvisor.Name = "btnManageAdvisor";
            this.btnManageAdvisor.Size = new System.Drawing.Size(183, 27);
            this.btnManageAdvisor.TabIndex = 15;
            this.btnManageAdvisor.Text = "    Manage Advisor";
            this.btnManageAdvisor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnManageAdvisor.UseTransparentBackground = true;
            this.btnManageAdvisor.Click += new System.EventHandler(this.btnManageAdvisor_Click);
            // 
            // btnAddAdvisor
            // 
            this.btnAddAdvisor.Animated = true;
            this.btnAddAdvisor.BackColor = System.Drawing.Color.Transparent;
            this.btnAddAdvisor.DefaultAutoSize = true;
            this.btnAddAdvisor.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnAddAdvisor.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnAddAdvisor.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnAddAdvisor.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnAddAdvisor.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddAdvisor.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnAddAdvisor.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnAddAdvisor.ForeColor = System.Drawing.Color.White;
            this.btnAddAdvisor.IndicateFocus = true;
            this.btnAddAdvisor.Location = new System.Drawing.Point(0, 0);
            this.btnAddAdvisor.Name = "btnAddAdvisor";
            this.btnAddAdvisor.Size = new System.Drawing.Size(183, 27);
            this.btnAddAdvisor.TabIndex = 12;
            this.btnAddAdvisor.Text = "    Add Advisor";
            this.btnAddAdvisor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnAddAdvisor.UseTransparentBackground = true;
            this.btnAddAdvisor.Click += new System.EventHandler(this.btnAddAdvisor_Click);
            // 
            // btnAdvisors
            // 
            this.btnAdvisors.Animated = true;
            this.btnAdvisors.BackColor = System.Drawing.Color.Transparent;
            this.btnAdvisors.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnAdvisors.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnAdvisors.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnAdvisors.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnAdvisors.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdvisors.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.btnAdvisors.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnAdvisors.ForeColor = System.Drawing.Color.White;
            this.btnAdvisors.Image = ((System.Drawing.Image)(resources.GetObject("btnAdvisors.Image")));
            this.btnAdvisors.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnAdvisors.ImageSize = new System.Drawing.Size(35, 35);
            this.btnAdvisors.IndicateFocus = true;
            this.btnAdvisors.Location = new System.Drawing.Point(0, 269);
            this.btnAdvisors.Name = "btnAdvisors";
            this.btnAdvisors.Size = new System.Drawing.Size(183, 47);
            this.btnAdvisors.TabIndex = 13;
            this.btnAdvisors.Text = "Advisors";
            this.btnAdvisors.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnAdvisors.UseTransparentBackground = true;
            this.btnAdvisors.Click += new System.EventHandler(this.btnAdvisors_Click);
            // 
            // pnlSudentSubMenu
            // 
            this.pnlSudentSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.pnlSudentSubMenu.Controls.Add(this.btnManageStudents);
            this.pnlSudentSubMenu.Controls.Add(this.btnAddStudent);
            this.pnlSudentSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSudentSubMenu.Location = new System.Drawing.Point(0, 208);
            this.pnlSudentSubMenu.Name = "pnlSudentSubMenu";
            this.pnlSudentSubMenu.Size = new System.Drawing.Size(183, 61);
            this.pnlSudentSubMenu.TabIndex = 12;
            // 
            // btnManageStudents
            // 
            this.btnManageStudents.Animated = true;
            this.btnManageStudents.BackColor = System.Drawing.Color.Transparent;
            this.btnManageStudents.DefaultAutoSize = true;
            this.btnManageStudents.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnManageStudents.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnManageStudents.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnManageStudents.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnManageStudents.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManageStudents.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnManageStudents.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnManageStudents.ForeColor = System.Drawing.Color.White;
            this.btnManageStudents.IndicateFocus = true;
            this.btnManageStudents.Location = new System.Drawing.Point(0, 27);
            this.btnManageStudents.Name = "btnManageStudents";
            this.btnManageStudents.Size = new System.Drawing.Size(183, 27);
            this.btnManageStudents.TabIndex = 15;
            this.btnManageStudents.Text = "    Manage Students";
            this.btnManageStudents.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnManageStudents.UseTransparentBackground = true;
            this.btnManageStudents.Click += new System.EventHandler(this.btnManageStudents_Click);
            // 
            // btnAddStudent
            // 
            this.btnAddStudent.Animated = true;
            this.btnAddStudent.BackColor = System.Drawing.Color.Transparent;
            this.btnAddStudent.DefaultAutoSize = true;
            this.btnAddStudent.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnAddStudent.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnAddStudent.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnAddStudent.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnAddStudent.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddStudent.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnAddStudent.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStudent.ForeColor = System.Drawing.Color.White;
            this.btnAddStudent.IndicateFocus = true;
            this.btnAddStudent.Location = new System.Drawing.Point(0, 0);
            this.btnAddStudent.Name = "btnAddStudent";
            this.btnAddStudent.Size = new System.Drawing.Size(183, 27);
            this.btnAddStudent.TabIndex = 12;
            this.btnAddStudent.Text = "    Add Student";
            this.btnAddStudent.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnAddStudent.UseTransparentBackground = true;
            this.btnAddStudent.Click += new System.EventHandler(this.guna2Button1_Click);
            // 
            // btnStudent
            // 
            this.btnStudent.Animated = true;
            this.btnStudent.BackColor = System.Drawing.Color.Transparent;
            this.btnStudent.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnStudent.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnStudent.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnStudent.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnStudent.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnStudent.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.btnStudent.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnStudent.ForeColor = System.Drawing.Color.White;
            this.btnStudent.Image = ((System.Drawing.Image)(resources.GetObject("btnStudent.Image")));
            this.btnStudent.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnStudent.ImageSize = new System.Drawing.Size(35, 35);
            this.btnStudent.IndicateFocus = true;
            this.btnStudent.Location = new System.Drawing.Point(0, 161);
            this.btnStudent.Name = "btnStudent";
            this.btnStudent.Size = new System.Drawing.Size(183, 47);
            this.btnStudent.TabIndex = 11;
            this.btnStudent.Text = "Students";
            this.btnStudent.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnStudent.UseTransparentBackground = true;
            this.btnStudent.Click += new System.EventHandler(this.btnStudent_Click_1);
            // 
            // btnExit
            // 
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.btnExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(22)))), ((int)(((byte)(34)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.ForeColor = System.Drawing.Color.Silver;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(0, 1089);
            this.btnExit.Name = "btnExit";
            this.btnExit.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnExit.Size = new System.Drawing.Size(183, 45);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "  Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlMenuHeader
            // 
            this.pnlMenuHeader.Controls.Add(this.guna2PictureBox2);
            this.pnlMenuHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenuHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlMenuHeader.Name = "pnlMenuHeader";
            this.pnlMenuHeader.Size = new System.Drawing.Size(183, 161);
            this.pnlMenuHeader.TabIndex = 0;
            // 
            // guna2PictureBox2
            // 
            this.guna2PictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2PictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox2.Image")));
            this.guna2PictureBox2.ImageRotate = 0F;
            this.guna2PictureBox2.Location = new System.Drawing.Point(0, 0);
            this.guna2PictureBox2.Name = "guna2PictureBox2";
            this.guna2PictureBox2.Size = new System.Drawing.Size(183, 161);
            this.guna2PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox2.TabIndex = 1;
            this.guna2PictureBox2.TabStop = false;
            // 
            // pnlChildForm
            // 
            this.pnlChildForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.pnlChildForm.Controls.Add(this.guna2PictureBox1);
            this.pnlChildForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlChildForm.Location = new System.Drawing.Point(200, 0);
            this.pnlChildForm.Name = "pnlChildForm";
            this.pnlChildForm.Size = new System.Drawing.Size(1150, 691);
            this.pnlChildForm.TabIndex = 1;
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2PictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox1.Image")));
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.Location = new System.Drawing.Point(470, 235);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(337, 310);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox1.TabIndex = 0;
            this.guna2PictureBox1.TabStop = false;
            // 
            // msgBox
            // 
            this.msgBox.Buttons = Guna.UI2.WinForms.MessageDialogButtons.OK;
            this.msgBox.Caption = null;
            this.msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.None;
            this.msgBox.Parent = null;
            this.msgBox.Style = Guna.UI2.WinForms.MessageDialogStyle.Dark;
            this.msgBox.Text = null;
            // 
            // btnGroupEvaluationStatus
            // 
            this.btnGroupEvaluationStatus.Animated = true;
            this.btnGroupEvaluationStatus.BackColor = System.Drawing.Color.Transparent;
            this.btnGroupEvaluationStatus.DefaultAutoSize = true;
            this.btnGroupEvaluationStatus.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnGroupEvaluationStatus.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnGroupEvaluationStatus.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnGroupEvaluationStatus.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnGroupEvaluationStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGroupEvaluationStatus.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(42)))));
            this.btnGroupEvaluationStatus.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnGroupEvaluationStatus.ForeColor = System.Drawing.Color.White;
            this.btnGroupEvaluationStatus.IndicateFocus = true;
            this.btnGroupEvaluationStatus.Location = new System.Drawing.Point(0, 162);
            this.btnGroupEvaluationStatus.Name = "btnGroupEvaluationStatus";
            this.btnGroupEvaluationStatus.Size = new System.Drawing.Size(183, 27);
            this.btnGroupEvaluationStatus.TabIndex = 20;
            this.btnGroupEvaluationStatus.Text = "    Group wise Evaluations";
            this.btnGroupEvaluationStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btnGroupEvaluationStatus.UseTransparentBackground = true;
            this.btnGroupEvaluationStatus.Click += new System.EventHandler(this.btnGroupEvaluationStatus_Click);
            // 
            // formMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1350, 691);
            this.Controls.Add(this.pnlChildForm);
            this.Controls.Add(this.pnlSideBar);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "formMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.formMain_Load);
            this.pnlSideBar.ResumeLayout(false);
            this.pnlReportSubMenu.ResumeLayout(false);
            this.pnlReportSubMenu.PerformLayout();
            this.pnlGroupEvaluationSubMenu.ResumeLayout(false);
            this.pnlGroupEvaluationSubMenu.PerformLayout();
            this.pnlEvaluationSubMenu.ResumeLayout(false);
            this.pnlEvaluationSubMenu.PerformLayout();
            this.pnlGroupSubMenu.ResumeLayout(false);
            this.pnlGroupSubMenu.PerformLayout();
            this.pnlProjectSubMenu.ResumeLayout(false);
            this.pnlProjectSubMenu.PerformLayout();
            this.pnlAdvisorSubMenu.ResumeLayout(false);
            this.pnlAdvisorSubMenu.PerformLayout();
            this.pnlSudentSubMenu.ResumeLayout(false);
            this.pnlSudentSubMenu.PerformLayout();
            this.pnlMenuHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).EndInit();
            this.pnlChildForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSideBar;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel pnlMenuHeader;
        private Guna.UI2.WinForms.Guna2Button btnStudent;
        private System.Windows.Forms.Panel pnlSudentSubMenu;
        private Guna.UI2.WinForms.Guna2Button btnAddStudent;
        private Guna.UI2.WinForms.Guna2Button btnManageStudents;
        private System.Windows.Forms.Panel pnlChildForm;
        private System.Windows.Forms.Panel pnlAdvisorSubMenu;
        private Guna.UI2.WinForms.Guna2Button btnManageAdvisor;
        private Guna.UI2.WinForms.Guna2Button btnAddAdvisor;
        private Guna.UI2.WinForms.Guna2Button btnAdvisors;
        private System.Windows.Forms.Panel pnlProjectSubMenu;
        private Guna.UI2.WinForms.Guna2Button btnManagaeProject;
        private Guna.UI2.WinForms.Guna2Button btnAddProject;
        private Guna.UI2.WinForms.Guna2Button btnProject;
        private System.Windows.Forms.Panel pnlGroupSubMenu;
        private Guna.UI2.WinForms.Guna2Button btnDeleteGroup;
        private Guna.UI2.WinForms.Guna2Button btnCreateGroup;
        private Guna.UI2.WinForms.Guna2Button btnGroups;
        private Guna.UI2.WinForms.Guna2Button btnAssignProject;
        private System.Windows.Forms.Panel pnlEvaluationSubMenu;
        private Guna.UI2.WinForms.Guna2Button btnManageEvaluations;
        private Guna.UI2.WinForms.Guna2Button btnCreateEvaluation;
        private Guna.UI2.WinForms.Guna2Button btnEvaluation;
        private System.Windows.Forms.Panel pnlGroupEvaluationSubMenu;
        private Guna.UI2.WinForms.Guna2Button btnEditGroupEvaluation;
        private Guna.UI2.WinForms.Guna2Button btnMarkEvaluation;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox2;
        private Guna.UI2.WinForms.Guna2Button btnGroupEvaluation;
        private Guna.UI2.WinForms.Guna2Button btnReports;
        private System.Windows.Forms.Panel pnlReportSubMenu;
        private Guna.UI2.WinForms.Guna2Button btnAllAdvisors;
        private Guna.UI2.WinForms.Guna2Button btnAllStudents;
        private Guna.UI2.WinForms.Guna2MessageDialog msgBox;
        private Guna.UI2.WinForms.Guna2Button btnProjectsNotAssignedToAnyGroup;
        private Guna.UI2.WinForms.Guna2Button btnNonGroupStudents;
        private Guna.UI2.WinForms.Guna2Button btnNonProjectAdvisor;
        private Guna.UI2.WinForms.Guna2Button btnEvaluationStatus;
        private Guna.UI2.WinForms.Guna2Button btnGroupEvaluationStatus;
    }
}

