﻿using MidProject.BL;
using MidProject.DAL;
using MidProject.GroupEvaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidProject
{
    public partial class formEditGroupEvaluation : Form
    {
        GroupProjectDL groupProjectDL;
        ProjectDL projectDL;
        formSearchDialog formSearch;
        Panel pnlChildForm;
        public formEditGroupEvaluation(Panel pnlChildForm)
        {
            groupProjectDL = new GroupProjectDL();
            projectDL = new ProjectDL();
            this.pnlChildForm = pnlChildForm;
            InitializeComponent();
        }


        private void openChildForm(Form childForm)
        {
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            pnlChildForm.Controls.Add(childForm);
            pnlChildForm.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (DGV.RowCount == 0)
                return;
            int rowIdx = DGV.SelectedRows[0].Index;
            string groupId = DGV.Rows[rowIdx].Cells["GroupId"].Value.ToString();
            openChildForm(new formEditGroupEvaluation_SelectEvaluation(groupId));
        }
        private void btnSearch_Click_1(object sender, EventArgs e)
        {
            formSearch = new formSearchDialog("Enter GroupId ID");
            formSearch.searchClick += new EventHandler(onSearch);
            formSearch.ShowDialog();
        }
        private void onSearch(object sender, EventArgs e)
        {
            bool isFound = false;
            string ID = formSearch.RegNo; //  form return ID
            for (int i = 0; i < DGV.RowCount; i++)
            {
                // finding row that contain reg no
                if (DGV.Rows[i].Cells["GroupId"].Value.ToString() == ID)
                {
                    DGV.ClearSelection();
                    DGV.Rows[i].Selected = true;
                    sbVertical.Value = i + 1;
                    isFound = true;
                    break;
                }
            }
            formSearch.isFound = isFound;

        }

        private void formEditGroupEvaluation_Load(object sender, EventArgs e)
        {
            DGV.MultiSelect = false;
            DGV.ScrollBars = ScrollBars.None;
            if (!groupProjectDL.fetchRecords())
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
            }
            DGV.DataSource = groupProjectDL.getList();
            changeDGVorder();
        }
        private void changeDGVorder()
        {
            //DGV.AutoGenerateColumns = false;

            DataTable dt = new DataTable();
            List<GroupProject> lst = (List<GroupProject>)DGV.DataSource;
            dt.Columns.Add("GroupId", typeof(string));
            dt.Columns.Add("ProjectId", typeof(string));
            dt.Columns.Add("projectTitle", typeof(string));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("AssignmentDate", typeof(string));
            foreach (var item in lst)
            {
                string projectId = item.ProjectId;
                projectDL.fetchById(projectId);
                List<Project> lst1 = projectDL.getList();
                string title = "";
                string description = "";
                foreach (var item1 in lst1)
                {
                    title = item1.Title;
                    description = item1.Description;
                }
                DataRow dr = dt.NewRow();
                dr["GroupId"] = item.GroupId;
                dr["ProjectId"] = projectId;
                dr["projectTitle"] = title;
                dr["Description"] = description;
                dr["AssignmentDate"] = item.AssignmentDate;
                dt.Rows.Add(dr);
                DGV.DataSource = dt;
                DGV.Columns["GroupId"].DisplayIndex = 0;
                DGV.Columns["ProjectId"].DisplayIndex = 1;
                DGV.Columns["projectTitle"].DisplayIndex = 2;
                DGV.Columns["Description"].DisplayIndex = 3;
                DGV.Columns["AssignmentDate"].DisplayIndex = 4;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
