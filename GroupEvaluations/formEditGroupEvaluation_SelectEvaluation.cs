﻿using MidProject.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidProject.GroupEvaluations
{
    public partial class formEditGroupEvaluation_SelectEvaluation : Form
    {
        formEditGroupEvaluationDialog formMarkEvaluationDialog;
        GroupEvaluationDL groupEvaluationDL;
        string groupId;
        public formEditGroupEvaluation_SelectEvaluation(string groupId)
        {
            groupEvaluationDL = new GroupEvaluationDL();
            this.groupId = groupId;
            InitializeComponent();
        }
        private void formEditEvaluation_SelectEvaluation_Load(object sender, EventArgs e)
        {

            DGV.MultiSelect = false;
            DGV.ScrollBars = ScrollBars.None;
            bindData();
        }
        void bindData()
        {
            DGV.DataSource = null;
            DataTable dt = groupEvaluationDL.fetchGroupEvaluationsDataTable(groupId);
            if (dt == null)
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
            }
            /*  if (!groupEvaluationDL.fetchGroupEvaluations(groupId))
              {
                  msgBox.Caption = Messages.CAPTION_FAIL;
                  msgBox.Text = Messages.FAIL_RETRIEVE;
                  msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                  msgBox.Show();
                  this.Close();
              }
              DGV.DataSource = groupEvaluationDL.getList();*/

            dt.Columns.Add("ObtainedMarks", typeof(string));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["ObtainedMarks"] = groupEvaluationDL.getObtainedMarks(groupId, dt.Rows[i]["Id"].ToString());

            }
            DGV.DataSource = dt;
            foreach (DataGridViewColumn column in DGV.Columns)
            {
                column.ReadOnly = true;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

            if (DGV.RowCount == 0)
                return;
            int rowIdx = DGV.SelectedRows[0].Index;

            formMarkEvaluationDialog = new formEditGroupEvaluationDialog(groupId, DGV.Rows[rowIdx].Cells["Id"].Value.ToString(),
                                                                         DGV.Rows[rowIdx].Cells["TotalMarks"].Value.ToString(),
                                                                         DGV.Rows[rowIdx].Cells["TotalWeightage"].Value.ToString(),
                                                                         groupEvaluationDL.getObtainedMarks(groupId,
                                                                         DGV.Rows[rowIdx].Cells["Id"].Value.ToString()));
            formMarkEvaluationDialog.onSuccess += new EventHandler(onSuccessEvaluation);
            formMarkEvaluationDialog.ShowDialog();
        }
        private void onSuccessEvaluation(object sender, EventArgs e)
        {
            bindData();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
