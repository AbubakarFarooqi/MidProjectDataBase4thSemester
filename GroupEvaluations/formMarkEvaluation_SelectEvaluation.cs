﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formMarkEvaluation_SelectEvaluation : Form
    {
        formMarkEvaluationDialog formMarkEvaluationDialog;
        GroupEvaluationDL groupEvaluationDL;
        string groupId;
        public formMarkEvaluation_SelectEvaluation(string groupId)
        {
            groupEvaluationDL = new GroupEvaluationDL();
            this.groupId = groupId;
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formMarkEvaluation_SelectEvaluation_Load(object sender, EventArgs e)
        {

            DGV.MultiSelect = false;
            DGV.ScrollBars = ScrollBars.None;
            if (!groupEvaluationDL.fetchPendingEvaluations(groupId))
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
            }
            DGV.DataSource = groupEvaluationDL.getList();
            //changeDGVorder();
        }
        void bindData()
        {
            DGV.DataSource = null;
            if (!groupEvaluationDL.fetchPendingEvaluations(groupId))
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
            }
            DGV.DataSource = groupEvaluationDL.getList();
        }
        private void btnSelect_Click(object sender, EventArgs e)
        {

            if (DGV.RowCount == 0)
                return;
            int rowIdx = DGV.SelectedRows[0].Index;
            formMarkEvaluationDialog = new formMarkEvaluationDialog(groupId, DGV.Rows[rowIdx].Cells["Id"].Value.ToString(), DGV.Rows[rowIdx].Cells["TotalMarks"].Value.ToString(), DGV.Rows[rowIdx].Cells["TotalWeightage"].Value.ToString());
            formMarkEvaluationDialog.onSuccess += new EventHandler(onSuccessEvaluation);
            formMarkEvaluationDialog.ShowDialog();
        }

        private void onSuccessEvaluation(object sender, EventArgs e)
        {
            bindData();
        }
    }
}
