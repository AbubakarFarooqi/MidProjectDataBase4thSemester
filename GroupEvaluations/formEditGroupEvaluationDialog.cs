﻿using MidProject.BL;
using MidProject.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidProject.GroupEvaluations
{
    public partial class formEditGroupEvaluationDialog : Form
    {
        string groupId;
        string evaluationId;
        GroupEvaluationDL groupEvaluationDL;
        public event EventHandler onSuccess;
        public formEditGroupEvaluationDialog(string groupId, string evaluationId, string totalMarks, string weightage, string obtainedMarks)
        {
            InitializeComponent();
            this.txtTotalMarks.Text = totalMarks;
            this.txtWeightage.Text = weightage;
            this.txtObtainedMarks.Text = obtainedMarks;
            this.txtTotalMarks.ReadOnly = true;
            this.txtWeightage.ReadOnly = true;
            this.evaluationId = evaluationId;
            this.groupId = groupId;
            groupEvaluationDL = new GroupEvaluationDL();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Guna.UI2.WinForms.Guna2TextBox[] TXTs = new Guna.UI2.WinForms.Guna2TextBox[5];
            ErrorProvider[] EPs = new ErrorProvider[5];
            TXTs[0] = txtObtainedMarks;
            EPs[0] = epObtainedMarks;
            if (ValidationsAndParsings.isEveryFieldSet(TXTs, EPs, 1)) // check all fields must be filled
            {
                if (ValidationsAndParsings.isNumber(txtObtainedMarks.Text))
                {
                    if (int.Parse(txtObtainedMarks.Text) <= int.Parse(txtTotalMarks.Text))
                    {
                        if (!groupEvaluationDL.updateObatinedMarks(groupId, evaluationId, txtObtainedMarks.Text))
                        {
                            msgBox.Caption = Messages.CAPTION_FAIL;
                            msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                            msgBox.Text = Messages.FAIL_UPDATE;
                            msgBox.Show();


                        }
                        else
                        {
                            msgBox.Caption = Messages.CAPTION_SUCCESS;
                            msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                            msgBox.Text = Messages.SUCCESS_UPDATE;
                            msgBox.Show();
                            onSuccess?.Invoke(this, null);
                            this.Close();
                        }
                    }
                    else
                    {
                        epObtainedMarks.SetError(txtObtainedMarks, Messages.NOT_ACCEPTABLE_OBTAINED_MARKS);
                    }
                }
                else
                {
                    epObtainedMarks.SetError(txtObtainedMarks, Messages.NOT_NUMBER);

                }
            }
        }
    }
}
