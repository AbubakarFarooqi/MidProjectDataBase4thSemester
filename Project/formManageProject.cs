﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formManageProject : Form
    {
        ProjectDL projectDL;
        formSearchDialog formSearch;
        formUpdateProjectDialog formUpdate;

        public formManageProject()
        {
            projectDL = new ProjectDL();
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            formSearch = new formSearchDialog("Enter ID");
            formSearch.searchClick += new EventHandler(onSearch);
            formSearch.ShowDialog();
        }
        private void onSearch(object sender, EventArgs e)
        {
            bool isFound = false;
            string ID = formSearch.RegNo; //  form return ID
            for (int i = 0; i < DGV.RowCount; i++)
            {
                // finding row that contain reg no
                if (DGV.Rows[i].Cells[0].Value.ToString() == ID)
                {
                    DGV.ClearSelection();
                    DGV.Rows[i].Selected = true;
                    sbVertical.Value = i + 1;
                    isFound = true;
                    break;
                }
            }
            formSearch.isFound = isFound;

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (DGV.RowCount == 0)
                return;
            int rowIdx = DGV.SelectedRows[0].Index;
            string id = DGV.Rows[rowIdx].Cells["Id"].Value.ToString();
            string title = DGV.Rows[rowIdx].Cells["Title"].Value.ToString();
            string description = DGV.Rows[rowIdx].Cells["Description"].Value.ToString();
            formUpdate = new formUpdateProjectDialog(new Project(id, description, title));
            formUpdate.onSuccessUpdate += new EventHandler(onSuccessUpdate);
            formUpdate.ShowDialog();
        }
        private void onSuccessUpdate(object sender, EventArgs e)
        {
            updateDGV();
        }
        private void updateDGV()
        {
            projectDL.fetchRecords();
            DGV.DataSource = projectDL.getList();
            changeDGVorder();
        }
        private void formManageProject_Load(object sender, EventArgs e)
        {
            DGV.MultiSelect = false;
            DGV.ScrollBars = ScrollBars.None;
            if (!projectDL.fetchRecords())
            {
                msgBox.Caption = Messages.CAPTION_FAIL;
                msgBox.Text = Messages.FAIL_RETRIEVE;
                msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                msgBox.Show();
                this.Close();
            }
            DGV.DataSource = projectDL.getList();
            changeDGVorder();
        }
        private void changeDGVorder()
        {
            DGV.AutoGenerateColumns = false;
            DGV.Columns["Id"].DisplayIndex = 0;
            DGV.Columns["Title"].DisplayIndex = 1;
            DGV.Columns["Description"].DisplayIndex = 2;
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}
