﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formAddProject : Form
    {
        ProjectDL projectDL;
        public formAddProject()
        {
            projectDL = new ProjectDL();
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            Guna.UI2.WinForms.Guna2TextBox[] TXTs = new Guna.UI2.WinForms.Guna2TextBox[2];
            ErrorProvider[] EPs = new ErrorProvider[2];
            TXTs[0] = txtDescription;
            TXTs[1] = txtTitle;
            EPs[0] = epDescription;
            EPs[1] = epTitle;
            if (ValidationsAndParsings.isEveryFieldSet(TXTs, EPs, 2))
            {
                Project project = new Project(txtDescription.Text, txtTitle.Text);
                if (projectDL.insert(project))
                {
                    msgBox.Caption = Messages.CAPTION_SUCCESS;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                    msgBox.Text = Messages.SUCCESS_INSERT;
                    msgBox.Show();
                }
                else
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Text = Messages.FAIL_INSERT;
                    msgBox.Show();
                }
            }
        }

        private void formAddProject_Load(object sender, EventArgs e)
        {

        }
    }
}
