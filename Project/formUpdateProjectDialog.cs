﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject.BL;
using MidProject.DAL;
namespace MidProject
{
    public partial class formUpdateProjectDialog : Form
    {
        public event EventHandler onSuccessUpdate;
        Project project;
        ProjectDL projectDL;
        public formUpdateProjectDialog(Project project)
        {
            InitializeComponent();
            projectDL = new ProjectDL();
            this.project = project;
            txtDescription.Text = project.Description;
            txtTitle.Text = project.Title;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Guna.UI2.WinForms.Guna2TextBox[] TXTs = new Guna.UI2.WinForms.Guna2TextBox[5];
            ErrorProvider[] EPs = new ErrorProvider[5];
            TXTs[0] = txtTitle;
            TXTs[1] = txtDescription;
            EPs[0] = epTitle;
            EPs[1] = epDescription;
            if (ValidationsAndParsings.isEveryFieldSet(TXTs, EPs, 2)) // check all fields must be filled
            {
                project.Title = txtTitle.Text;
                project.Description = txtDescription.Text;
                if (projectDL.update(project))
                {
                    msgBox.Caption = Messages.CAPTION_SUCCESS;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Information;
                    msgBox.Text = Messages.SUCCESS_UPDATE;
                    msgBox.Show();
                    onSuccessUpdate?.Invoke(this, null);
                    this.Close();
                }
                else
                {
                    msgBox.Caption = Messages.CAPTION_FAIL;
                    msgBox.Icon = Guna.UI2.WinForms.MessageDialogIcon.Error;
                    msgBox.Text = Messages.FAIL_UPDATE;
                    msgBox.Show();
                }
            }
        }
    }
}
