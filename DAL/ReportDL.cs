﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidProject.DAL
{
    public class ReportDL : CRUD
    {
        public DataTable getAllStudents()
        {
            Dictionary<string, string> columns = new Dictionary<string, string>();
            columns["student.Id"] = "ID";
            columns["student.RegistrationNo"] = "[Registration Number]";
            columns["person.FirstName"] = "[First Name]";
            columns["person.LastName"] = "[Last Name]";
            columns["person.Contact"] = "[Contact]";
            columns["person.Email"] = "[Email]";
            columns["CONVERT(VARCHAR(10), person.DateOfBirth , 101)"] = "[Date Of Birth]";
            columns["(select value from Lookup where id = person.Gender)"] = "Gender";
            DataTable dt = base.retrieveWithjoin("student", "person", columns, "student.Id", "person.Id");
            if (dt == null)
                return null;
            return dt;
        }

        public DataTable getAllAdvisors()
        {
            Dictionary<string, string> columns = new Dictionary<string, string>();

            columns["advisor.Id"] = "ID";
            columns["person.FirstName"] = "[First Name]";
            columns["person.LastName"] = "[Last Name]";
            columns["person.Contact"] = "[Contact]";
            columns["person.Email"] = "[Email]";
            columns["(select value from Lookup where id = advisor.Designation)"] = "[Designation]";
            columns["advisor.Salary"] = "[Salary]";
            columns["CONVERT(VARCHAR(10), person.DateOfBirth , 101)"] = "[Date Of Birth]";
            columns["(select value from Lookup where id = person.Gender)"] = "Gender";


            DataTable dt = base.retrieveWithjoin("advisor", "person", columns, "advisor.Id", "person.Id");

            if (dt == null)
                return null;
            return dt;
        }

        public DataTable getNonGroupProject()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select p.Id , p.Title , p.Description " +
                                            "from Project p " +
                                            "where p.id not in ( select gp.ProjectId from GroupProject gp)", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable getNonGroupStudents()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select s.Id , s.RegistrationNo as [Registration Number], p.FirstName , p.LastName ,p.Contact , p.Email , CONVERT(VARCHAR(10), p.DateOfBirth , 101) as [Date of Birth] , " +
                                                "(select value from Lookup where id = p.Gender)as Gender " +
                                                "from Student s " +
                                                "join Person p " +
                                                "on s.Id = p.Id " +
                                                "where s.Id not in " +
                                                "(Select GroupStudent.StudentId from GroupStudent where Status= 3)", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable getNonProjectAdvisors()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select a.Id , p.FirstName , p.LastName ," +
                                                "p.Contact  ,p.Email ,(select value from Lookup where id = a.Designation) as Designation," +
                                                " a.Salary,CONVERT(VARCHAR(10), p.DateOfBirth , 101) as [Date of Birth] ," +
                                                " (select value from Lookup where id = p.Gender)as Gender " +
                                                "from Advisor a " +
                                                "join Person p " +
                                                "on a.Id = p.Id " +
                                                "where a.Id not in (Select ProjectAdvisor.AdvisorId from ProjectAdvisor)", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable getEvaluationsNames()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select name  , Id " +
                                                "from Evaluation ev", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException ee)
            {
                return null;
            }
        }

        public DataTable getGroupIds()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select Id " +
                                                "from [Group]", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException ee)
            {
                return null;
            }
        }

        public bool isEvaluated(string groupId, string evaluationId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select  " +
                                                "case when exists(select groupid  " +
                                                "					  from GroupEvaluation  " +
                                                "					  where GroupId = @groupId AND EvaluationId = @evaluationId)  " +
                                                "					  then  'exist' else 'not'  " +
                                                "					  end as gp " +
                                                "from GroupEvaluation "
                                               , con);
                cmd.Parameters.AddWithValue("@groupId", groupId);
                cmd.Parameters.AddWithValue("@evaluationId", evaluationId);
                if (cmd.ExecuteScalar().ToString() == "exist")
                    return true;
                return false;
            }
            catch (SqlException ee)
            {
                return false;
            }
        }


        public string getEvaluatedmarks(string groupId, string evaluationId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select case when exists(select groupid  from GroupEvaluation  " +
                                                " where GroupId = @groupId AND EvaluationId = @evaluationId)    " +
                                                " then  (select concat(gv.ObtainedMarks , ' / ' , e.TotalMarks)as a   " +
                                                " from GroupEvaluation gv join Evaluation e on e.Id = gv.EvaluationId  " +
                                                " where EvaluationId = @evaluationId AND GroupId = @groupId) else 'Not Marked'    " +
                                                "					  end as gp " +
                                                "from GroupEvaluation "
                                               , con);
                cmd.Parameters.AddWithValue("@groupId", groupId);
                cmd.Parameters.AddWithValue("@evaluationId", evaluationId);
                string marks = cmd.ExecuteScalar().ToString();
                return marks;
            }
            catch (SqlException ee)
            {
                return null;
            }
        }

        public DataTable getGroupstudent(string groupId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select CONCAT(p.FirstName , ' ', p.LastName ) as name " +
                                                "from GroupStudent g " +
                                                "join person p  " +
                                                "on p.Id = g.StudentId  " +
                                                "where GroupId = @groupId "
                                               , con);
                cmd.Parameters.AddWithValue("@groupId", groupId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException ee)
            {
                return null;
            }
        }
    }
}
