﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MidProject.BL;
namespace MidProject.DAL
{
    public class EvaluationDL : CRUD
    {
        List<Evaluation> lst;
        public bool insert(Evaluation evaluation)
        {
            Dictionary<int, string> columns = new Dictionary<int, string>();
            columns[0] = evaluation.Name;
            columns[1] = evaluation.Totalmarks;
            columns[2] = evaluation.TotalWeightage;
            return base.insert("Evaluation", columns);
        }
        public bool update(Evaluation evaluation)
        {
            Dictionary<string, string> columns = new Dictionary<string, string>();
            columns["Name"] = evaluation.Name;
            columns["TotalMarks"] = evaluation.Totalmarks;
            columns["TotalWeightage"] = evaluation.TotalWeightage;
            return base.update("evaluation", columns, "id", evaluation.Id);
        }
        public bool isAcceptableWeightage(string weightage)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select sum(totalWeightage) from Evaluation", con);
                string dbWehtage = cmd.ExecuteScalar().ToString();
                if (int.Parse(weightage) + int.Parse(dbWehtage) > 100)
                    return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool isAcceptableWeightageUpdate(string id, string weightage)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select sum(totalWeightage) from Evaluation", con);
                string dbWehtage = cmd.ExecuteScalar().ToString();
                cmd = new SqlCommand("select totalWeightage from Evaluation where id = \'" + id + "\'", con);
                string dbWehtageExisting = cmd.ExecuteScalar().ToString();
                dbWehtage = (int.Parse(dbWehtage) - int.Parse(dbWehtageExisting)).ToString();
                if (int.Parse(weightage) + int.Parse(dbWehtage) > 100)
                    return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool fetchRecords()
        {
            lst = new List<Evaluation>();
            Dictionary<string, string> columns = new Dictionary<string, string>();

            columns["Id"] = "ID";
            columns["Name"] = "[Name]";
            columns["TotalMarks"] = "[Total Marks]";
            columns["TotalWeightage"] = "[Total Weightage]";

            DataTable dt = base.retrieve("Evaluation", columns);

            if (dt == null)
                return false;
            fillList(dt);
            return true;
        }
        private void fillList(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string id = dt.Rows[i].ItemArray[0].ToString();
                string name = dt.Rows[i].ItemArray[1].ToString();
                string totalMarks = dt.Rows[i].ItemArray[2].ToString();
                string totalWeightage = dt.Rows[i].ItemArray[3].ToString();
                Evaluation evaluation = new Evaluation(id, name, totalMarks, totalWeightage);
                lst.Add(evaluation);

            }

        }
        public List<Evaluation> getList()
        {
            return lst;
        }




    }
}
