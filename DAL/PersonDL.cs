﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace MidProject.DAL
{
    class PersonDL : CRUD
    {
        protected string getGenderValue(string gender)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select value from Lookup where id = @Gender", con);
                cmd.Parameters.AddWithValue("@Gender", gender);
                string id = cmd.ExecuteScalar().ToString();
                return id;
            }
            catch (SqlException ex)
            {
                return null;
            }


        }
        protected int getGenderID(string gender)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select id  from Lookup where Value = @Gender", con);
                cmd.Parameters.AddWithValue("@Gender", gender);
                int id = int.Parse(cmd.ExecuteScalar().ToString());
                return id;
            }
            catch (SqlException ex)
            {
                return -1;
            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="contact"></param>
        /// <param name="email"></param>
        /// <param name="DOB"></param>
        /// <param name="gender"></param>
        /// <returns>
        /// Id of inserted Person or null in case of error
        /// </returns>
        protected string insert(string firstName, string lastName, string contact
                        , string email, string DOB, string gender)
        {

            Dictionary<int, string> columns = new Dictionary<int, string>();
            columns[0] = firstName;
            columns[1] = lastName;
            columns[2] = contact;
            columns[3] = email;
            columns[4] = DOB;
            columns[5] = getGenderID(gender).ToString();
            string Id = base.insertAndGet("Person", columns, "Id");
            return Id;
        }
        protected bool update(string id, string firstName, string lastName, string contact
                        , string email, string DOB, string gender)
        {
            Dictionary<string, string> columns = new Dictionary<string, string>();
            columns["FirstName"] = firstName;
            columns["LastName"] = lastName;
            columns["Contact"] = contact;
            columns["Email"] = email;
            columns["DateOfBirth"] = DOB;
            columns["Gender"] = getGenderID(gender).ToString();
            if (base.update("person", columns, "id", id))
                return true;
            return false;
        }
        protected bool delete(string id)
        {
            return base.delete("person", "id", id);
        }
    }
}
