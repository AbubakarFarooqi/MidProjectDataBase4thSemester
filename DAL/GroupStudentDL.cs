﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MidProject.BL;
using System.Data;

namespace MidProject.DAL
{
    class GroupStudentDL : CRUD
    {
        List<Student> lst;
        /// <summary>
        /// Check whether a student is part of any group
        /// </summary>
        /// <param name="id"></param>
        public bool isGroupStudent(string id)
        {
            SqlCommand cmd = new SqlCommand();
            string query = "Select StudentId from groupStudent where StudentId = " + id;
            cmd.CommandText = query;
            cmd.Connection = con;
            //try
            // {
            object x = cmd.ExecuteScalar();
            if (x == null)
                return false;
            // }
            //catch (Exception e)
            // {
            return true;
            //}
        }
        public bool insert(GroupStudent std)
        {
            Dictionary<int, string> columns = new Dictionary<int, string>();
            columns[0] = std.GroupId;
            columns[1] = std.StudentId;
            columns[2] = getStatus(std.Status).ToString();
            columns[3] = std.AssignmentDate;
            return base.insert("Groupstudent", columns);
        }
        private int getStatus(string status)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select id  from Lookup where Value = @status", con);
                cmd.Parameters.AddWithValue("@status", status);
                int designationId = int.Parse(cmd.ExecuteScalar().ToString());
                return designationId;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }
        public DataTable fetchRecords(string groupId)
        {
            lst = new List<Student>();
            Dictionary<string, string> columns = new Dictionary<string, string>();

            columns["groupStudent.StudentId"] = "StudentID";
            columns["groupStudent.GroupId"] = "GroupID";
            columns["groupStudent.Status"] = "Status";
            columns["person.FirstName"] = "[First Name]";
            columns["person.LastName"] = "[Last Name]";


            DataTable dt = base.retrieveWithjoin("groupStudent", "person", columns, "groupStudent.StudentId", "person.Id");
            DataTable dt2 = new DataTable();
            for (int i = 0; i < dt.Columns.Count; i++)
                dt2.Columns.Add(dt.Columns[i].ColumnName, dt.Columns[i].DataType);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i].ItemArray[1].ToString() == groupId && dt.Rows[i].ItemArray[2].ToString() == "3")
                    dt2.Rows.Add(dt.Rows[i].ItemArray[0], dt.Rows[i].ItemArray[1], dt.Rows[i].ItemArray[2], dt.Rows[i].ItemArray[3], dt.Rows[i].ItemArray[4]);
            }
            dt2.Columns.RemoveAt(1);
            dt2.Columns.RemoveAt(1);
            //fillList(dt);
            if (dt2 == null)
                return null;
            return dt2;
        }
        private void fillList(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string id = dt.Rows[i].ItemArray[0].ToString();
                string firstName = dt.Rows[i].ItemArray[1].ToString();
                string lastName = dt.Rows[i].ItemArray[2].ToString();
                Student student = new Student(id, firstName, lastName, "", "", "", "", "");
                lst.Add(student);
            }
        }

        public List<Student> getList()
        {
            return lst;
        }



    }
}
