﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Data;
using System.Collections;

namespace MidProject.DAL
{
    public class CRUD
    {
        protected SqlConnection con;

        public CRUD()
        {
            con = Configuration.getInstance().getConnection();
        }

        private string updateParameters(Dictionary<string, string> columns, SqlCommand cmd)
        {
            string parameters = "";
            int i = 0;
            foreach (KeyValuePair<string, string> item in columns)
            {
                string parName = "@" + i;
                i++;
                string val = item.Value;

                parameters += item.Key + "=" + parName + ",";
                cmd.Parameters.AddWithValue(parName, val);
            }
            parameters = parameters.Remove(parameters.Length - 1);
            /*  for(int i = 0; i < columns.Keys.Count; i++)
              {
                  string parName = "@" + i;
                  string val = columns[columns.Keys.];
              }*/
            return parameters;
        }
        private string insertParameters(Dictionary<int, string> columns, SqlCommand cmd)
        {
            string parameters = "";
            for (int i = 0; i < columns.Keys.Count; i++)
            {
                cmd.Parameters.AddWithValue("@" + i, columns[i]);
                parameters += "@" + i;
                if (i + 1 != columns.Keys.Count)
                    parameters += ",";
            }
            return parameters;

        }
        private string retrieveParameters(Dictionary<string, string> columns)
        {
            string parameters = "";

            // To get an IDictionaryEnumerator
            // for the SortedDictionary
            IDictionaryEnumerator myEnumerator =
                          columns.GetEnumerator();

            // If MoveNext passes the end of the
            // collection, the enumerator is positioned
            // after the last element in the collection
            // and MoveNext returns false.
            while (myEnumerator.MoveNext())
            {
                parameters += myEnumerator.Key + " AS " + myEnumerator.Value + ",";
            }
            /*foreach (KeyValuePair<string, string> item in columns)
            {
            }*/
            parameters = parameters.Remove(parameters.Length - 1);
            return parameters;

        }

        /// <summary>
        /// Insert into table and return specified column 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columns"></param>
        /// <param name="returningColumn"></param>
        /// <returns>
        ///     return specified column in case of error return null
        /// </returns>
        protected string insertAndGet(string tableName, Dictionary<int, string> columnsInOrder, string returningColumn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                string parameters = insertParameters(columnsInOrder, cmd);
                string query = "INSERT " + tableName + " OUTPUT INSERTED." + returningColumn + " values (" + parameters + ")";
                cmd.Connection = con;
                cmd.CommandText = query;
                string output = cmd.ExecuteScalar().ToString();
                return output;
            }
            catch (SqlException e)
            {
                return null;
            }
        }

        /// <summary>
        /// insert data in specified column
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columns"></param>
        /// <returns>
        /// true on success otherwise false;
        /// </returns>
        protected bool insert(string tableName, Dictionary<int, string> columnsInOrder)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                string parameters = insertParameters(columnsInOrder, cmd);
                string query = "INSERT INTO " + tableName + " values (" + parameters + ")";
                cmd.Connection = con;
                cmd.CommandText = query;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException e)
            {
                return false;
            }
        }




        /// <summary>
        /// Update Specified Columns
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columns"></param>
        /// <param name="conditionColumn"></param>
        /// <param name="conditionValue"></param>
        /// <returns></returns>


        protected bool update(string tableName, Dictionary<string, string> columns, string conditionColumn, string conditionValue)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                string parameters = updateParameters(columns, cmd);
                string query = "UPDATE " + tableName + " set " + parameters + " where " + conditionColumn + " = @value";
                cmd.Parameters.AddWithValue("@value", conditionValue);
                cmd.Connection = con;
                cmd.CommandText = query;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException e)
            {
                return false;
            }
        }

        /// <summary>
        /// retrieve records from database key = originalName , value = outputName
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columns"></param>
        /// <param name="conditionColumn"></param>
        /// <param name="conditionValue"></param>
        /// <returns></returns>
        protected DataTable retrieve(string tableName, Dictionary<string, string> columns, string conditionColumn, string conditionValue)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                string parameters = retrieveParameters(columns);
                string query = "SELECT " + parameters + " FROM " + tableName + " WHERE " + conditionColumn + "= @conditionValue";
                cmd.Parameters.AddWithValue("@conditionValue", conditionValue);
                cmd.Connection = con;
                cmd.CommandText = query;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException e)
            {
                return null;
            }
        }
        /// <summary>
        /// retrieve records from database key = originalName , value = outputName
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        protected DataTable retrieve(string tableName, Dictionary<string, string> columns)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                string parameters = retrieveParameters(columns);
                string query = "SELECT " + parameters + " FROM " + tableName;
                cmd.Connection = con;
                cmd.CommandText = query;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException e)
            {
                return null;
            }
        }

        protected bool delete(string tableName, string conditionColumn, string conditionValue)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                string query = "DELETE " + tableName + " where " + conditionColumn + " = @value";
                cmd.Connection = con;
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@value", conditionValue);
                cmd.ExecuteNonQuery();
                return true;
            }

            catch (SqlException e)
            {
                return false;
            }
        }

        protected DataTable retrieveWithjoin(string firstTableName, string secondTableName, Dictionary<string, string> columns, string joinConditionColumn1, string joinConditionColumn2)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                string parameters = retrieveParameters(columns);
                string query = "SELECT " + parameters + " FROM " + firstTableName +
                                " JOIN " + secondTableName + " ON " + joinConditionColumn1 + " = " + joinConditionColumn2;
                cmd.Connection = con;
                cmd.CommandText = query;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException e)
            {
                return null;
            }
        }

    }
}
