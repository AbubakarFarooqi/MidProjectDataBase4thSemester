﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MidProject.BL;
using System.Data;
namespace MidProject.DAL
{
    class StudentDL : PersonDL
    {
        List<Student> lst;
        public bool insert(Student student)
        {
            string Id = base.insert(student.FirstName, student.LastName, student.Contact,
                            student.Email, student.DateOfBirth, student.Gender);
            if (Id == null)
                return false;
            Dictionary<int, string> columns = new Dictionary<int, string>();
            columns[0] = Id;
            columns[1] = student.RegistrationNumber;
            return base.insert("Student", columns);
        }
        public bool update(Student student)
        {
            if (!base.update(student.Id, student.FirstName, student.LastName, student.Contact,
                            student.Email, student.DateOfBirth, student.Gender))
                return false;
            Dictionary<string, string> columns = new Dictionary<string, string>();
            columns["RegistrationNo"] = student.RegistrationNumber;
            if (!base.update("Student", columns, "id", student.Id))
                return false;
            return true;
        }
        /* public new bool delete(string id)
         {
             if (!base.delete("student", "id", id))
                 return false;
             return base.delete(id);
         }*/
        public bool fetchRecords()
        {
            lst = new List<Student>();
            Dictionary<string, string> columns = new Dictionary<string, string>();

            columns["student.Id"] = "ID";
            columns["student.RegistrationNo"] = "[Registration Number]";
            columns["person.FirstName"] = "[First Name]";
            columns["person.LastName"] = "[Last Name]";
            columns["person.Contact"] = "[Contact]";
            columns["person.Email"] = "[Email]";
            columns["CONVERT(VARCHAR(10), person.DateOfBirth , 101)"] = "[Date Of Birth]";
            columns["(select value from Lookup where id = person.Gender)"] = "Gender";


            DataTable dt = base.retrieveWithjoin("student", "person", columns, "student.Id", "person.Id");

            if (dt == null)
                return false;
            fillList(dt);
            return true;
        }

        public bool fetchNonGroupStudents()
        {
            lst = new List<Student>();
            SqlCommand cmd = new SqlCommand("select s.Id AS ID , s.RegistrationNo AS [Registration Number],p.FirstName AS [First Name] , p.LastName AS [Last Name]" +
                ", p.Contact AS Contact , p.Email AS Email , CONVERT(VARCHAR(10), p.DateOfBirth , 101) AS [Date Of birth],(select value from Lookup where id = p.Gender) AS Gender" +
                " from Student s join Person p on s.Id = p.Id where s.Id not in (select studentId from GroupStudent Where status = 3)", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            if (dt == null)
                return false;
            fillList(dt);
            return true;

        }
        private void fillList(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string id = dt.Rows[i].ItemArray[0].ToString();
                string regNo = dt.Rows[i].ItemArray[1].ToString();
                string firstName = dt.Rows[i].ItemArray[2].ToString();
                string lastName = dt.Rows[i].ItemArray[3].ToString();
                string contact = dt.Rows[i].ItemArray[4].ToString();
                string email = dt.Rows[i].ItemArray[5].ToString();
                string dob = dt.Rows[i].ItemArray[6].ToString();
                string gender = dt.Rows[i].ItemArray[7].ToString();
                Student student = new Student(id, firstName, lastName, contact, email, dob,
                                                gender, regNo);
                lst.Add(student);

            }
        }

        public List<Student> getList()
        {
            return lst;
        }
    }
}
